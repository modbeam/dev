package io.bitbucket.modbeam.ui.natures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import io.bitbucket.modbeam.ui.Activator;
import io.bitbucket.modbeam.ui.builders.ModBeamBuilder;
import io.bitbucket.modbeam.ui.properties.PropertiesPage;

public class ModBeamNature implements IProjectNature {

    public static final String NATURE_ID = Activator.PLUGIN_ID + ".modBeamNature";

    private IProject project;

    @Override
    public void configure() throws CoreException {
        IProjectDescription desc = project.getDescription();
        List<ICommand> builders = new ArrayList<>(Arrays.asList(desc.getBuildSpec()));

        if (!builders.stream().anyMatch(new Predicate<ICommand>() {

            @Override
            public boolean test(ICommand t) {
                return t.getBuilderName().equals(ModBeamBuilder.BUILDER_ID);
            }
        })) {

            ICommand command = desc.newCommand();
            command.setBuilderName(ModBeamBuilder.BUILDER_ID);
            builders.add(command);
            ICommand[] newBuilders = new ICommand[builders.size()];
            desc.setBuildSpec(builders.toArray(newBuilders));
            project.setDescription(desc, null);
        }
        
        configureBuildPath(project);
    }

    @Override
    public void deconfigure() throws CoreException {
        IProjectDescription desc = project.getDescription();
        List<ICommand> builders = new ArrayList<>(Arrays.asList(desc.getBuildSpec()));

        if (builders.stream().anyMatch(new Predicate<ICommand>() {

            @Override
            public boolean test(ICommand t) {
                return t.getBuilderName().equals(ModBeamBuilder.BUILDER_ID);
            }
        })) {

            ICommand command = desc.newCommand();
            command.setBuilderName(ModBeamBuilder.BUILDER_ID);
            builders.remove(command);
            ICommand[] newBuilders = new ICommand[builders.size()];
            desc.setBuildSpec(builders.toArray(newBuilders));
            project.setDescription(desc, null);
        }
    }

    @Override
    public IProject getProject() {
        return project;
    }

    @Override
    public void setProject(IProject project) {
        this.project = project;
    }

    public static void configureBuildPath(IProject project) {
        try {
            IPath generatedRoot = project.getFolder(PropertiesPage.getBytecodeOutput(project)).getLocation().makeRelativeTo(ResourcesPlugin.getWorkspace().getRoot().getLocation()).makeAbsolute();
            IJavaProject javaProject = JavaCore.create(project);
            List<IClasspathEntry> cpes = new LinkedList<>(Arrays.asList(javaProject.getRawClasspath()));
            IClasspathEntry generatedCpe = null;
            for (IClasspathEntry cpe : cpes) {
                if (cpe.getPath().equals(generatedRoot)) {
                    generatedCpe = cpe;
                }
            }

            if (PropertiesPage.getBytecodeOutputBuildpath(project) && generatedCpe == null) {
                    cpes.add(0, JavaCore.newLibraryEntry(generatedRoot, null, null, new IAccessRule[0], new IClasspathAttribute[0], false));
                    IClasspathEntry[] newClasspath = new IClasspathEntry[cpes.size()];
                    javaProject.setRawClasspath(cpes.toArray(newClasspath), null);
            }
            if (!PropertiesPage.getBytecodeOutputBuildpath(project) && generatedCpe != null) {
                    cpes.remove(generatedCpe);
                    IClasspathEntry[] newClasspath = new IClasspathEntry[cpes.size()];
                    javaProject.setRawClasspath(cpes.toArray(newClasspath), null);
            }
        } catch (JavaModelException e) {
            Activator.logThrowable(e);
        }
    }

}
