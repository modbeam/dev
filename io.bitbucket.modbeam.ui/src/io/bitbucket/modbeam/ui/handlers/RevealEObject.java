
package io.bitbucket.modbeam.ui.handlers;

import java.io.File;
import java.util.Collections;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.EContextService;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.ui.URIEditorInputFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.e4.compatibility.CompatibilityEditor;
import org.eclipse.xtext.nodemodel.ILeafNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.ILocationInFileProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.XtextDocumentUtil;
import org.eclipse.xtext.util.ITextRegion;
import org.eclipse.xtext.util.Pair;
import org.eclipse.xtext.util.Tuples;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import io.bitbucket.modbeam.jbc.presentation.JbcEditor;
import io.bitbucket.modbeam.jbc.provider.JbcEditPlugin;

public class RevealEObject {

    @Inject
    ESelectionService selectionService;

    // @Inject
    // private EObjectAtOffsetHelper eObjectAtOffsetHelper;
    //
    // @Inject
    // private ILocationInFileProvider locationInFileProvider;

    @Inject
    EModelService modelService;

    @Inject
    EContextService contextService;

    protected EObject selectedElement;

    @Execute
    public void execute() {
        IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
        try {
            JbcEditor editor = (JbcEditor) page.openEditor(new URIEditorInput(selectedElement.eResource().getURI()),
                    "io.bitbucket.modbeam.jbc.presentation.JbcEditorID");
            editor.setSelection(new StructuredSelection(selectedElement));
            Object input = editor.getViewer().getInput();
            String uri = selectedElement.eResource().getURIFragment(selectedElement);
            EObject eObjectInEditor = ((XtextResource) ((org.eclipse.emf.ecore.resource.ResourceSet) input)
                    .getResources().get(0)).getEObject(uri);
            // find(((XtextResource) ((org.eclipse.emf.ecore.resource.ResourceSet)
            // input).getResources().get(0)).getContents(), selectedElement);
            editor.setSelectionToViewer(Collections.singleton(eObjectInEditor));
            editor.setFocus();
        } catch (PartInitException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void find(EList<EObject> contents, EObject selectedElement2) {
        for (EObject eObject : contents) {
            if (eObject.equals(selectedElement2))
                System.out.println("found");
            find(eObject.eContents(), selectedElement2);
        }
    }

    @CanExecute
    public boolean canExecute() {
        selectedElement = null;
        
        IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
        Control control = editorPart.getAdapter(Control.class);

        final int offset;
        if (control instanceof StyledText) {
            StyledText text = (StyledText) control;
            offset = text.getCaretOffset();
        } else {
            return false;
        }

        if (editorPart != null) {
            ITextOperationTarget target = (ITextOperationTarget) editorPart.getAdapter(ITextOperationTarget.class);
            if (target instanceof ITextViewer) {
                ITextViewer textViewer = (ITextViewer) target;
                IXtextDocument xtextDocument = XtextDocumentUtil.get(textViewer);
                if (xtextDocument == null)
                    return false;

                xtextDocument.readOnly(new IUnitOfWork<Object, XtextResource>() {

                    @Override
                    public Object exec(XtextResource resource) throws Exception {
                        final EObjectAtOffsetHelper eObjectAtOffsetHelper = resource.getResourceServiceProvider()
                                .get(EObjectAtOffsetHelper.class);
                        selectedElement = eObjectAtOffsetHelper.resolveContainedElementAt(resource,
                                offset);
                        return null;
                    }
                });
            }
        }
        return selectedElement != null;
    }

}