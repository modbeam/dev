
package io.bitbucket.modbeam.ui.handlers;

import java.util.Collections;

import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.ITreeSelection;

import io.bitbucket.modbeam.ui.builders.ModBeamBuilder;

public class ExtractJbc {

    private IFile selectedClassFile;

    @Execute
    public void execute() {
        Job job = Job.create("Mod-BEAM Selective Build", new ICoreRunnable() {

            @Override
            public void run(IProgressMonitor monitor) throws CoreException {
                IProject project = selectedClassFile.getProject();
                ModBeamBuilder.buildFromClass(project, Collections.singletonList(selectedClassFile), monitor);
            }
        });
        job.setPriority(Job.BUILD);
        job.schedule();

    }

    @CanExecute
    public boolean canExecute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) ITreeSelection selection) {
        selectedClassFile = null;
        if (selection.size() != 1)
            return false;
        Object selected = selection.getFirstElement();
        if (selected instanceof IFile && ((IFile) selected).getFileExtension().equals("class")) {
            selectedClassFile = ((IFile) selected);
            return true;
        }
        else {
            return false;
        }
    }

}