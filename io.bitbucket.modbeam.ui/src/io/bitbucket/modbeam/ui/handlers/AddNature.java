package io.bitbucket.modbeam.ui.handlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Named;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;

import io.bitbucket.modbeam.ui.Activator;
import io.bitbucket.modbeam.ui.natures.ModBeamNature;


public class AddNature {
    
    private IJavaProject selectedJavaProject = null;
        
    @Execute
    public void execute() {
        try {
            IProject project = selectedJavaProject.getAdapter(IProject.class);
            IProjectDescription description = project.getDescription();

            List<String> natures = new ArrayList<String>(Arrays.asList(description.getNatureIds()));
            natures.add(ModBeamNature.NATURE_ID);
            String[] newNatures = new String[natures.size()];
            natures.toArray(newNatures);

            // validate the natures
            IWorkspace workspace = ResourcesPlugin.getWorkspace();
            IStatus status = workspace.validateNatureSet(newNatures);

            // only apply new nature, if the status is ok
            if (status.getCode() == IStatus.OK) {
                description.setNatureIds(newNatures);
                project.setDescription(description, null);
            }
            project.build(IncrementalProjectBuilder.FULL_BUILD, new NullProgressMonitor());
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
    }
    
    @CanExecute
    public boolean canExecute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) ISelection selection2) {
        Activator.logInfo("canExecut AddNature");
        selectedJavaProject = null;
        ITreeSelection selection = (ITreeSelection) selection2;
        Activator.logInfo("Selection Size: " + selection.size());
        if (selection.size() != 1)
            return false;
        Object selected = selection.getFirstElement();
        Activator.logInfo("Selected type: " + selected.getClass());
        if (!(selected instanceof IJavaProject))
            return false;
        else {
            IJavaProject javaProject = (IJavaProject) selected;
            Activator.logInfo("Selected Java Project: " + javaProject);
            try {
                boolean alreadyHasModBeamNature = Arrays.asList(javaProject.getAdapter(IProject.class).getDescription().getNatureIds()).contains(ModBeamNature.NATURE_ID);
                selectedJavaProject = javaProject;
                Activator.logInfo("Already has nature: " + alreadyHasModBeamNature);
                return !alreadyHasModBeamNature;
            } catch (CoreException e) {
                Activator.logThrowable(e);
                return false;
            }
        }
    }
    
}
