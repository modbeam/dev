package io.bitbucket.modbeam.ui.api;

import io.bitbucket.modbeam.ui.Activator;

public class TimeTracer {
    private long startTime;
    private long startTimeOverall;
    private StringBuilder timeTrace;
    
    public TimeTracer(String task) {
        timeTrace = new StringBuilder(task).append(": ");
        startTime = System.currentTimeMillis();
        startTimeOverall = startTime;
    }
    
    public void traceStep(String desc) {
        long time = System.currentTimeMillis() - startTime;
        timeTrace.append(desc).append(" [").append(time).append("ms] - ");
        startTime = System.currentTimeMillis();
    }

    public void traceEnd() {
        long time = System.currentTimeMillis() - startTimeOverall;
        timeTrace.append("DONE").append(" [total: ").append(time).append("ms]");
        startTime = Long.MIN_VALUE;
        Activator.logInfo(timeTrace.toString());
        timeTrace = null;
    }
}