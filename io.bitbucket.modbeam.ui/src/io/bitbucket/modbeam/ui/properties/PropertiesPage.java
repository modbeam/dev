package io.bitbucket.modbeam.ui.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;

import io.bitbucket.modbeam.ui.Activator;
import io.bitbucket.modbeam.ui.builders.ModBeamBuilder;
import io.bitbucket.modbeam.ui.natures.ModBeamNature;

public class PropertiesPage extends PropertyPage {

    private static final QualifiedName MODELS_OUTPUT_QN = new QualifiedName("io.bitbucket.modbeam", "modelsOutput");
    private static final QualifiedName AUTO_GEN_BYTECODE_QN = new QualifiedName("io.bitbucket.modbeam",
            "getAutoGenBytecode");
    private static final QualifiedName AUTO_GEN_MODELS_QN = new QualifiedName("io.bitbucket.modbeam",
            "getAutoGenModels");
    private static final QualifiedName BYTECODE_GEN_INPUT_QN = new QualifiedName("io.bitbucket.modbeam",
            "bytecodeGenInputOutput");
    private static final QualifiedName BYTECODE_OUTPUT_QN = new QualifiedName("io.bitbucket.modbeam", "bytecodeOutput");
    private static final QualifiedName BYTECODE_OUTPUT_BUILDPATH_QN = new QualifiedName("io.bitbucket.modbeam",
            "bytecodeOutputBuildpath");
    private static final QualifiedName ONE_CLASS_PER_MODEL_QN = new QualifiedName("io.bitbucket.modbeam",
            "oneClassPerModel");
    private static final QualifiedName MODEL_FILE_EXTENSION_QN = new QualifiedName("io.bitbucket.modbeam",
            "modelFileExtension");

    private static final String[] modelFileExtensions = {"jbc", "jbc-xmi"};
    
    private static final String MODELS_OUTPUT_DEFAULT = "jbc-models";
    private static final String BYTECODE_GEN_INPUT_DEFAULT = "jbc-models";
    private static final String BYTECODE_OUTPUT_DEFAULT = "jbc-gen";
    private static final String MODEL_FILE_EXTENSION_DEFAULT = modelFileExtensions[0];
    private static final boolean AUTO_GEN_MODELS_DEFAULT = true;
    private static final boolean AUTO_GEN_BYTECODE_DEFAULT = true;
    private static final boolean BYTECODE_OUTPUT_BUILDPATH_DEFAULT = true;
    private static final boolean ONE_CLASS_PER_MODEL_DEFAULT = true;

    private Text modelsOutputText;
    private Text bytecodeGenInputText;
    private Text bytecodeOutputText;
    private Combo modelFileExtensionDropdown;
    private Button autoGenModelsButton;
    private Button autoGenBytecodeButton;
    private Button bytecodeOutputBuildpathButton;
    private Button oneClassPerModelButton;
    
    /**
     * Constructor for SamplePropertyPage.
     */
    public PropertiesPage() {
        super();
    }

    /**
     * @see PreferencePage#createContents(Composite)
     */
    protected Control createContents(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(2, false);
        composite.setLayout(layout);
        GridData data = new GridData(GridData.FILL);
        data.grabExcessHorizontalSpace = true;
        composite.setLayoutData(data);

        new Label(composite, SWT.NULL).setText("Models output folder");
        modelsOutputText = new Text(composite, SWT.SINGLE | SWT.BORDER);
        modelsOutputText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

        autoGenModelsButton = new Button(composite, SWT.CHECK);
        autoGenModelsButton.setText("Auto generate JBC models for source folders");
        GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        gridData.horizontalSpan = 2;
        gridData.grabExcessHorizontalSpace = true;
        autoGenModelsButton.setLayoutData(gridData);

        new Label(composite, SWT.NULL).setText("Bytecode generation input folder");
        bytecodeGenInputText = new Text(composite, SWT.SINGLE | SWT.BORDER);
        bytecodeGenInputText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

        new Label(composite, SWT.NULL).setText("Bytecode output folder");
        bytecodeOutputText = new Text(composite, SWT.SINGLE | SWT.BORDER);
        bytecodeOutputText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

        autoGenBytecodeButton = new Button(composite, SWT.CHECK);
        autoGenBytecodeButton.setText("Auto generate bytecode for JBC Models");
        gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        gridData.horizontalSpan = 2;
        autoGenBytecodeButton.setLayoutData(gridData);

        bytecodeOutputBuildpathButton = new Button(composite, SWT.CHECK);
        bytecodeOutputBuildpathButton.setText("Prepend generated bytecode to build path");
        gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        gridData.horizontalSpan = 2;
        bytecodeOutputBuildpathButton.setLayoutData(gridData);

        oneClassPerModelButton = new Button(composite, SWT.CHECK);
        oneClassPerModelButton.setText("Generate one model for each class");
        gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        gridData.horizontalSpan = 2;
        oneClassPerModelButton.setLayoutData(gridData);

        new Label(composite, SWT.NULL).setText("Model file extension (jbc for textual)");
        modelFileExtensionDropdown = new Combo(composite, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
        for (String ext : modelFileExtensions) {
            modelFileExtensionDropdown.add(ext);
        }
        gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        modelFileExtensionDropdown.setLayoutData(gridData);

        
        IProject project = getElement().getAdapter(IProject.class);
        modelsOutputText.setText(getModelsOutput(project));
        autoGenModelsButton.setSelection(getAutoGenModels(project));
        bytecodeGenInputText.setText(getBytecodeGenInput(project));
        bytecodeOutputText.setText(getBytecodeOutput(project));
        autoGenBytecodeButton.setSelection(getAutoGenBytecode(project));
        bytecodeOutputBuildpathButton.setSelection(getBytecodeOutputBuildpath(project));
        oneClassPerModelButton.setSelection(getOneClassPerModel(project));
        setModelFileExtension(getModelFileExtension(project));

        return composite;
    }

    private void setModelFileExtension(String modelFileExtension) {
        for (int i = 0; i < modelFileExtensions.length; i++) {
            if (modelFileExtension.equals(modelFileExtensions[i]))
                modelFileExtensionDropdown.select(i);
        }
    }

    public static String getModelsOutput(IProject project) {
        try {
            String modelsOutputValue = project.getPersistentProperty(PropertiesPage.MODELS_OUTPUT_QN);
            if (modelsOutputValue != null) {
                return modelsOutputValue;
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return MODELS_OUTPUT_DEFAULT;
    }

    public static String getBytecodeGenInput(IProject project) {
        try {
            String bytecodeGenInputValue = project.getPersistentProperty(PropertiesPage.BYTECODE_GEN_INPUT_QN);
            if (bytecodeGenInputValue != null) {
                return bytecodeGenInputValue;
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return BYTECODE_GEN_INPUT_DEFAULT;
    }

    public static String getBytecodeOutput(IProject project) {
        try {
            String bytecodeOutputValue = project.getPersistentProperty(PropertiesPage.BYTECODE_OUTPUT_QN);
            if (bytecodeOutputValue != null) {
                return bytecodeOutputValue;
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return BYTECODE_OUTPUT_DEFAULT;
    }

    public static boolean getAutoGenModels(IProject project) {
        try {
            String gutoGenModelsValue = project.getPersistentProperty(PropertiesPage.AUTO_GEN_MODELS_QN);
            if (gutoGenModelsValue != null) {
                return Boolean.parseBoolean(gutoGenModelsValue);
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return AUTO_GEN_MODELS_DEFAULT;
    }

    public static boolean getAutoGenBytecode(IProject project) {
        try {
            String autoGenBytecodeValue = project.getPersistentProperty(PropertiesPage.AUTO_GEN_BYTECODE_QN);
            if (autoGenBytecodeValue != null) {
                return Boolean.parseBoolean(autoGenBytecodeValue);
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return AUTO_GEN_BYTECODE_DEFAULT;
    }

    public static void setAutoGenBytecode(IProject project, boolean value) {
        try {
            project.setPersistentProperty(PropertiesPage.AUTO_GEN_BYTECODE_QN, Boolean.toString(value));
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
    }

    public static boolean getBytecodeOutputBuildpath(IProject project) {
        try {
            String bytecodeOutputBuildpath = project.getPersistentProperty(PropertiesPage.BYTECODE_OUTPUT_BUILDPATH_QN);
            if (bytecodeOutputBuildpath != null) {
                return Boolean.parseBoolean(bytecodeOutputBuildpath);
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return BYTECODE_OUTPUT_BUILDPATH_DEFAULT;

    }

    public static boolean getOneClassPerModel(IProject project) {
        try {
            String oneClassPerModel = project.getPersistentProperty(PropertiesPage.ONE_CLASS_PER_MODEL_QN);
            if (oneClassPerModel != null) {
                return Boolean.parseBoolean(oneClassPerModel);
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return ONE_CLASS_PER_MODEL_DEFAULT;

    }
    
    public static String getModelFileExtension(IProject project) {
        try {
            String modelFileExtension = project.getPersistentProperty(PropertiesPage.MODEL_FILE_EXTENSION_QN);
            if (modelFileExtension != null) {
                return modelFileExtension;
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        return MODEL_FILE_EXTENSION_DEFAULT;
    }



    protected void performDefaults() {
        super.performDefaults();
        modelsOutputText.setText(MODELS_OUTPUT_DEFAULT);
        bytecodeGenInputText.setText(BYTECODE_GEN_INPUT_DEFAULT);
        bytecodeOutputText.setText(BYTECODE_OUTPUT_DEFAULT);
        autoGenModelsButton.setSelection(AUTO_GEN_MODELS_DEFAULT);
        autoGenBytecodeButton.setSelection(AUTO_GEN_BYTECODE_DEFAULT);
        bytecodeOutputBuildpathButton.setSelection(BYTECODE_OUTPUT_BUILDPATH_DEFAULT);
        oneClassPerModelButton.setSelection(ONE_CLASS_PER_MODEL_DEFAULT);
        setModelFileExtension(MODEL_FILE_EXTENSION_DEFAULT);
    }

    public boolean performOk() {
        // store the value in the owner text field
        try {
            IProject project = getElement().getAdapter(IProject.class);
            project.setPersistentProperty(MODELS_OUTPUT_QN, modelsOutputText.getText());
            project.setPersistentProperty(AUTO_GEN_MODELS_QN, Boolean.toString(autoGenModelsButton.getSelection()));
            project.setPersistentProperty(BYTECODE_GEN_INPUT_QN, bytecodeGenInputText.getText());
            project.setPersistentProperty(BYTECODE_OUTPUT_QN, bytecodeOutputText.getText());
            project.setPersistentProperty(AUTO_GEN_BYTECODE_QN, Boolean.toString(autoGenBytecodeButton.getSelection()));
            project.setPersistentProperty(BYTECODE_OUTPUT_BUILDPATH_QN,
                    Boolean.toString(bytecodeOutputBuildpathButton.getSelection()));

            final String modelFileExtensionBefore = getModelFileExtension(project);
            project.setPersistentProperty(MODEL_FILE_EXTENSION_QN,
                    modelFileExtensionDropdown.getText());

            final boolean oneClassPerModelBefore = getOneClassPerModel(project);

            project.setPersistentProperty(ONE_CLASS_PER_MODEL_QN,
                    Boolean.toString(oneClassPerModelButton.getSelection()));

            ModBeamNature.configureBuildPath(project);
            Job job = Job.create("Mod-BEAM Full Build", new ICoreRunnable() {

                @Override
                public void run(IProgressMonitor monitor) throws CoreException {
                    if (oneClassPerModelBefore != getOneClassPerModel(project) ||
                            !modelFileExtensionBefore.equals(getModelFileExtension(project)))
                        ModBeamBuilder.clean(monitor, project);
                    ModBeamBuilder.fullBuild(monitor, project);
                }
            });
            job.setPriority(Job.BUILD);
            job.schedule();

        } catch (CoreException e) {
            return false;
        }
        return true;
    }

}