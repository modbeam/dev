package io.bitbucket.modbeam.ui.builders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import io.bitbucket.modbeam.importexport.Exporter;
import io.bitbucket.modbeam.importexport.Extractor;
import io.bitbucket.modbeam.jbc.Annotation;
import io.bitbucket.modbeam.jbc.Clazz;
import io.bitbucket.modbeam.jbc.Field;
import io.bitbucket.modbeam.jbc.JbcFactory;
import io.bitbucket.modbeam.jbc.Method;
import io.bitbucket.modbeam.jbc.Project;
import io.bitbucket.modbeam.ui.Activator;
import io.bitbucket.modbeam.ui.api.TimeTracer;
import io.bitbucket.modbeam.ui.properties.PropertiesPage;

public class ModBeamBuilder extends IncrementalProjectBuilder {

    public static final String BUILDER_ID = Activator.PLUGIN_ID + ".modBeamBuilder";

    public ModBeamBuilder() {
    }

    @Override
    protected void clean(IProgressMonitor monitor) throws CoreException {
        super.clean(monitor);
        clean(monitor, getProject());
    }
    

    public static void clean(IProgressMonitor monitor, IProject project) throws CoreException {
        IFolder bytecodeGenFolder = project.getFolder(PropertiesPage.getBytecodeOutput(project));
        
        IFolder modelOutputFolder = project.getFolder(PropertiesPage.getModelsOutput(project));
        
        bytecodeGenFolder.delete(true, false, monitor);
        modelOutputFolder.delete(true, false, monitor);
        
        ensureFolders(monitor, project);
    }

    private static void ensureFolders(IProgressMonitor monitor, IProject project) throws CoreException {
        IFolder bytecodeGenFolder = project.getFolder(PropertiesPage.getBytecodeOutput(project));
        IFolder modelOutputFolder = project.getFolder(PropertiesPage.getModelsOutput(project));
        if (!bytecodeGenFolder.exists())
        	bytecodeGenFolder.create(true, true, monitor);
        if (!modelOutputFolder.exists())
        	modelOutputFolder.create(true, true, monitor);

    }
    
    @Override
    protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor) throws CoreException {
        ensureFolders(monitor, getProject());

        if (kind == IncrementalProjectBuilder.FULL_BUILD) {
            fullBuild(monitor, getProject());
        } else {
            IResourceDelta delta = getDelta(getProject());
            if (PropertiesPage.getOneClassPerModel(getProject()) == false || delta == null) {
                fullBuild(monitor, getProject());
            } else {
                incrementalBuild(delta, monitor);
            }
        }
        return null;
    }

    public void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) {
        try {
            List<IFile> modifiedClassFiles = new ArrayList<IFile>();
            List<IFile> modifiedJbcFiles = new ArrayList<IFile>();
            IJavaProject javaProject = JavaCore.create(getProject());
            IPath bytecodeGenInputFolder = getProject().getFolder(PropertiesPage.getBytecodeGenInput(getProject()))
                    .getLocation();
            delta.accept(new IResourceDeltaVisitor() {

                public boolean visit(IResourceDelta delta) {
                    IResource resource = delta.getResource();
                    if (resource instanceof IFile) {
                        IFile file = (IFile) resource;
                        if (delta.getResource().getName().endsWith(".class") && isSourceOutput(javaProject, resource)) {
                            modifiedClassFiles.add(file);
                        }
                        if (delta.getResource().getName().endsWith("." + PropertiesPage.getModelFileExtension(getProject()))
                                && bytecodeGenInputFolder.isPrefixOf(file.getLocation())) {
                            modifiedJbcFiles.add(file);
                        }
                    }
                    return true; // visit children too
                }
            });
            if (PropertiesPage.getAutoGenModels(getProject()))
                buildFromClass(getProject(), modifiedClassFiles, monitor);
            if (PropertiesPage.getAutoGenBytecode(getProject()))
                buildFromJbc(getProject(), modifiedJbcFiles, monitor);
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
    }

    protected boolean isSourceOutput(IJavaProject javaProject, IResource resource) {
        try {
            IClasspathEntry[] cpes = javaProject.getRawClasspath();
            for (IClasspathEntry cpe : cpes) {
                if (cpe.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                    IPath outputLocation = cpe.getOutputLocation();
                    if (outputLocation == null)
                        outputLocation = javaProject.getOutputLocation().makeAbsolute();
                    if (outputLocation.isPrefixOf(resource.getFullPath())) {
                        return true;
                    }
                }
            }
        } catch (JavaModelException e) {
            Activator.logThrowable(e);
        }
        return false;
    }

    public static void fullBuild(IProgressMonitor monitor, IProject project) {
        try {
            final List<IFile> classFiles = new ArrayList<IFile>();
            final IJavaProject javaProject = JavaCore.create(project);
            List<IPath> outputFolders = new ArrayList<>();
            IClasspathEntry[] cpes = javaProject.getRawClasspath();
            for (IClasspathEntry cpe : cpes) {
                if (cpe.getEntryKind() == IClasspathEntry.CPE_SOURCE && cpe.getOutputLocation() != null) {
                    outputFolders.add(cpe.getOutputLocation());
                }
            }
            if (javaProject.getOutputLocation() != null)
                outputFolders.add(javaProject.getOutputLocation());

            for (IPath path : outputFolders) {
                ResourcesPlugin.getWorkspace().getRoot().getFolder(path).accept(new IResourceVisitor() {

                    @Override
                    public boolean visit(IResource resource) throws CoreException {
                        if (resource instanceof IFile &&
                        		resource.getFileExtension() != null &&
                        		resource.getFileExtension().equals("class")) {
                            classFiles.add((IFile) resource);
                        }
                        return true; // visit children too
                    }
                });
            }

            if (PropertiesPage.getAutoGenModels(project))
                buildFromClass(project, classFiles, monitor);

            final List<IFile> jbcFiles = new ArrayList<IFile>();
            final IFolder bytecodeGenInputFolder = project.getFolder(PropertiesPage.getBytecodeGenInput(project));
            final IPath bytecodeGenInputPath = bytecodeGenInputFolder.getLocation();

            bytecodeGenInputFolder.accept(new IResourceVisitor() {

                public boolean visit(IResource resource) {
                    if (resource instanceof IFile) {
                        IFile file = (IFile) resource;
                        if (resource.getName().endsWith("." + PropertiesPage.getModelFileExtension(project))
                                && bytecodeGenInputPath.isPrefixOf(file.getLocation())) {
                            jbcFiles.add(file);
                        }
                    }
                    return true;
                }
            });

            if (PropertiesPage.getAutoGenBytecode(project))
                buildFromJbc(project, jbcFiles, monitor);
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
    }
    
    public static Set<String> getExcludesAnnotations() {
		// TODO
		// If the class is a TestNG test, we do not want to create a model for it.
		// Otherwise the Eclipse TestNG plugin would not be able to execute the
		// test anymore: 
		// Das Problem liegt meiner Meinung nach darin, dass das TestNG Plugin
		// erwartet, dass die Test-Klasse im Quellcode vorliegt. ModBeam verändert
		// aber den Build Path so, dass die Klasse jetzt zuerst in dem Class Folder
		// gefunden wird. Und dahin wird sie von ModBeam generiert, weil ein
		// Modell für die Test-Klasse existiert.
    	
    	// TODO implement more flexible mechanism that can be configured through the UI 

    	return Stream.of("org.testng.annotations.Test", "org.junit.Test", "org.junit.jupiter.api.Test",
    			"org.junit.jupiter.api.DynamicTest", "org.junit.jupiter.api.RepeatedTest")
    		.map(c -> "L" + c.replaceAll("\\.", "/") + ";")
    		.collect(Collectors.toSet());
    }

    private static boolean containsExcludedAnnotation(EList<Annotation> annotations,
			Set<String> excludedAnnotations) {

    	return annotations.stream().map(a -> a.getType().getTypeDescriptor())
    		.anyMatch(t -> excludedAnnotations.contains(t));
	}
   

    public static void buildFromClass(IProject project, List<IFile> classFiles, IProgressMonitor monitor) {
        IFolder outputFolder = project.getFolder(PropertiesPage.getModelsOutput(project));
        List<IFile> generatedJbcFiles = new ArrayList<>();
        Project jbcProject = JbcFactory.eINSTANCE.createProject();
        try {
        	Set<String> excludedAnnotations = getExcludesAnnotations();
        	
            classes: for (IFile classFile : classFiles) {
                Clazz c = new Extractor().extract(classFile.getContents());

                if (containsExcludedAnnotation(c.getRuntimeVisibleAnnotations(), excludedAnnotations) 
                		|| containsExcludedAnnotation(c.getRuntimeInvisibleAnnotations(), excludedAnnotations)) {
                	continue classes;
                }
                for (Method m : c.getMethods()) {
                    if (containsExcludedAnnotation(m.getRuntimeVisibleAnnotations(), excludedAnnotations) 
                    		|| containsExcludedAnnotation(m.getRuntimeInvisibleAnnotations(), excludedAnnotations)) {
                    	continue classes;
                    }
                }
                for (Field f : c.getFields()) {
                    if (containsExcludedAnnotation(f.getRuntimeVisibleAnnotations(), excludedAnnotations) 
                    		|| containsExcludedAnnotation(f.getRuntimeInvisibleAnnotations(), excludedAnnotations)) {
                    	continue classes;
                    }
                }
                
                jbcProject.getClasses().add(c);
                
                if (PropertiesPage.getOneClassPerModel(project)) {
                    writeModelFile(monitor, outputFolder, generatedJbcFiles, jbcProject, c.getName() + "." + PropertiesPage.getModelFileExtension(project));    
                    jbcProject = JbcFactory.eINSTANCE.createProject();
                }
            }
            if (!PropertiesPage.getOneClassPerModel(project)) {
                writeModelFile(monitor, outputFolder, generatedJbcFiles, jbcProject, "all-jbc-models." + PropertiesPage.getModelFileExtension(project));    
                jbcProject = JbcFactory.eINSTANCE.createProject();
            }
        } catch (IOException | CoreException e) {
            Activator.logThrowable(e);
        }
        buildFromJbc(project, generatedJbcFiles, monitor);
    }

	private static void writeModelFile(IProgressMonitor monitor, IFolder outputFolder, List<IFile> generatedJbcFiles,
            Project jbcProject, String outputFilename) throws IOException, CoreException {
        // Obtain a new resource set
        ResourceSet resSet = new ResourceSetImpl();
   
        IFile outputFile = outputFolder.getFile(outputFilename);
        // create an EMF resource
        Resource resource = resSet.createResource(URI.createURI(outputFile.getLocationURI().toString()));
        resource.getContents().add(jbcProject);
   
        // save the model
        resource.save(Collections.emptyMap());
        outputFile.refreshLocal(IResource.DEPTH_ZERO, monitor);
        outputFile.setDerived(true, monitor);
        generatedJbcFiles.add(outputFile);
    }

	
	// FIXME Deadlock
    public synchronized static void buildFromJbc(IProject project, List<IFile> modifiedJbcFiles, IProgressMonitor monitor) {
        TimeTracer tracer = new TimeTracer("generate bytecode");
        IFolder generatedRootDir = project.getFolder(PropertiesPage.getBytecodeOutput(project));
        tracer.traceStep("get outputfolder");
        try {
            for (IFile iFile : modifiedJbcFiles) {
                ResourceSet resSet = new ResourceSetImpl();
                Resource resource;
                if (iFile.getLocation() == null) {
                    resource = resSet.createResource(URI.createURI(URIUtil.toURI(iFile.getFullPath()).toString()));
                } else {
                    resource = resSet.createResource(URI.createURI(URIUtil.toURI(iFile.getLocation()).toString()));
                }
                tracer.traceStep("get input resource");
                try {
                    Map<Object, Object> loadOptions = Collections.emptyMap();
                    resource.load(loadOptions);
                } catch (IOException e) {
                    Activator.logThrowable(e);
                    continue;
                }
                tracer.traceStep("load options");

                EList<EObject> contents = resource.getContents();
                if (contents.size() != 1) {
                    Activator.logThrowable(new IllegalArgumentException("Did not find exactly one root in " + iFile));
                    continue;
                }
                tracer.traceStep("get model (project)");

                Project jbcProject = (Project) contents.get(0);
                for (Clazz clazz : jbcProject.getClasses()) {
                    String generatedFile = new Exporter().export(clazz, generatedRootDir.getLocation().toOSString());
                    tracer.traceStep("generate bytecode");
                    IResource generatedResource = generatedRootDir.getFile(Path.fromOSString(generatedFile));
                    tracer.traceStep("get output resource");
                    generatedResource.refreshLocal(IResource.DEPTH_ZERO, monitor);
                    tracer.traceStep("refresh output resource");
                    generatedResource.setDerived(true, monitor);
                    tracer.traceStep("set output resource derived");
                }
            }
        } catch (CoreException e) {
            Activator.logThrowable(e);
        }
        tracer.traceEnd();
    }

}
