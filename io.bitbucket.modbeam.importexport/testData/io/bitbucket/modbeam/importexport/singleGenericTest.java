package io.bitbucket.modbeam.importexport;

public class singleGenericTest {

	public static class Box<Type>{
		private Type t;
		
		public void set(Type t) { this.t = t; }
		public Type get() { return t; }
	}
	
	public static void main(String[] args){
		
		Box<String> test1 = new Box<>();
		Box<Integer> test2 = new Box<>();
		
		test1.set("String");
		test2.set(1);
		
		System.out.println(test1.get());
		System.out.println(test2.get());
	}
}
