package io.bitbucket.modbeam.importexport;

public class instanceofPatternTest {

	
	public static void main(String[] args){
		
		Object obj1 = "Test1";
		Object obj2 = 2;
		Object obj3 = "T3";
		Object obj4 = true;
		
		Object[] obj = {obj1, obj2, obj3, obj4};
		
		for(Object o: obj) {
			if(o instanceof String s) {
				System.out.println(s.toUpperCase());
			}
			else {
				System.out.println(o.getClass().toString());
			}
		}
	}
}
