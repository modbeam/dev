package io.bitbucket.modbeam.importexport;

public class switchExpressionPopulate {

	public enum Day {
	    MON, TUE, WED, FRI, SUN
	    }

	public static Day day1 = Day.MON;
	public static Day day2 = Day.TUE;
	public static Day day3 = Day.WED;
	
	
	public static void numLetter(Day day) {
	int numLetters = switch (day) {
		case MON, FRI, SUN -> 6;
		case TUE -> 7;
		default -> {
			String s = day.toString();
			int result = s.length();
			yield result;
			}
		};
		System.out.println(numLetters);
	}
		
	public static void main(String[] args) {
		numLetter(day1);
		numLetter(day2);
		numLetter(day3);
	}
}
