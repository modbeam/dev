package io.bitbucket.modbeam.importexport;

public class aLotOfGenericsTest {

	static public class BigBox<ElementType, KeyType, ValueType, Numberr, T, S>{
		private ElementType et;
		private KeyType kt;
		private ValueType vt;
		private Numberr n;
		private T t;
		private S s;
		
		
		public void setAll(ElementType et, KeyType kt, ValueType vt, Numberr n, T t, S s) {
			this.et = et;
			this.kt = kt;
			this.vt = vt;
			this.n = n;
			this.t = t;
			this.s = s;
		}
		
		public ElementType getE() 	{ return et; }
		public KeyType getK() 		{ return kt; }
		public ValueType getV() 	{ return vt; }
		public Numberr getN() 		{ return n; }
		public T getT() 			{ return t; }
		public S getS() 			{ return s; }
		
		public void printAll() {
			System.out.println(et.getClass().toString());
			System.out.println(kt.getClass().toString());
			System.out.println(vt.getClass().toString());
			System.out.println(n.getClass().toString());
			System.out.println(t.getClass().toString());
			System.out.println(s.getClass().toString());
		}
	}
	
	public static void main(String[] args){
		
		BigBox<Object, String, Long, Integer, Float, Float> test1 = new BigBox<>();
		
		test1.setAll("Object", "Element", 2147483648L, 2147483647, 0.125F, 0.00000000006F);
		
		test1.printAll();
	}
}
