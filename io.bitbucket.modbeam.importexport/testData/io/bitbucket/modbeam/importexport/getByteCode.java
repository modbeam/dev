package io.bitbucket.modbeam.importexport;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.nio.file.Paths;

public class getByteCode {

	private static String getClassCodeText(String path) throws InterruptedException, IOException {
		Process proc = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
//				.command("javap", "-p", "-c", "-constants", path).start();
				.command("javap", "-p", "-c", "-v", "-constants", path).start();
		String result = new String(proc.getInputStream().readAllBytes()).replaceAll("#[0-9]*", "##");
		proc.waitFor();
		
		return result;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		String path = "bin\\io\\bitbucket\\modbeam\\importexport\\";
		
		String[] files = {"singleGenericTest$Box", "genericClassDefTest$Entry", "aLotOfGenericsTest$BigBox"};

		for(String s: files) {
			Files.write(Paths.get("Output\\GenericBC\\" + s + ".txt"), getClassCodeText(path + s + ".class").getBytes());
		}
		
	}
}
