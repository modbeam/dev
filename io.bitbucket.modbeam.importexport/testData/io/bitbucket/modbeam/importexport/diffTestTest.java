package io.bitbucket.modbeam.importexport;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import org.apache.commons.text.diff.CommandVisitor;
import org.apache.commons.text.diff.StringsComparator;

public class diffTestTest {
		
	@SuppressWarnings("unused")
	private static boolean compare(String classFile1, String classFile2) throws IOException, InterruptedException {
		Process proc = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
				.command("javap", "-p", "-c", "-constants", classFile1).start();
//		.command("javap", "-p", "-c", "-v", "-constants", "bin/bcdiff/A.class").start();
		
		Process proc2 = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
				.command("javap", "-p", "-c", "-constants", classFile2).start();
		
		String result = new String(proc.getInputStream().readAllBytes()).replaceAll("#[0-9]*", "##");
		String result2 = new String(proc2.getInputStream().readAllBytes()).replaceAll("#[0-9]*", "##");
		/*
		System.out.println("output 1 was: ");
		System.out.println(result);
		System.out.println("------");

		System.out.println("output 2 was: ");
		System.out.println(result2);
		System.out.println("------");
		*/
		proc.waitFor();
		proc2.waitFor();
		
		return(result.equals(result2));
    }
	
	private static String compS(String path) throws InterruptedException, IOException {
		Process proc = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
				.command("javap", "-p", "-c", "-constants", path).start();
//		.command("javap", "-p", "-c", "-v", "-constants", "bin/bcdiff/A.class").start();
		
		String result = new String(proc.getInputStream().readAllBytes()).replaceAll("#[0-9]*", "##");
		/*
		System.out.println("output 1 was: ");
		System.out.println(result);
		System.out.println("------");
		*/
		proc.waitFor();
		
		return result;
	}
			
	public static void main(String[] args) throws IOException, InterruptedException {
		
		String s1 = compS("bin\\io\\bitbucket\\modbeam\\importexport\\helloWorld.class");
		String s2 = compS("bin\\io\\bitbucket\\modbeam\\importexport\\helloWorld2.class");
		
		//boolean test = compare(s1, s1);
		
		//System.out.println(test);
		
		// Create a diff comparator with two inputs strings.
		StringsComparator comparator = new StringsComparator(s1, s2);
		
		// Initialize custom visitor and visit char by char.
		MyCommandsVisitor myCommandsVisitor = new MyCommandsVisitor();
		
		comparator.getScript().visit(myCommandsVisitor);
		
		// Print final diff.
		System.out.println("FINAL DIFF = " + myCommandsVisitor.left + " | " + myCommandsVisitor.right);		
	}
}

/*
 * Custom visitor.
 */
class MyCommandsVisitor implements CommandVisitor<Character> {
 
	String left = "";
	String right = "";
 
	@Override
	public void visitKeepCommand(Character c) {
		// Character is present in both files.
		left = left + c;
		right = right + c;
	}
 
	@Override
	public void visitInsertCommand(Character c) {
		/*
		 * Character is present in right file but not in left. Method name
		 * 'InsertCommand' means, c need to insert it into left to match right.
		 */
		right = right + "(" + c + ")";
	}
 
	@Override
	public void visitDeleteCommand(Character c) {
		/*
		 * Character is present in left file but not right. Method name 'DeleteCommand'
		 * means, c need to be deleted from left to match right.
		 */
		left = left + "{" + c + "}";
	}
}
