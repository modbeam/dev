package io.bitbucket.modbeam.importexport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class tryWithRessourcesTest {

	public static void main(String[] args) throws IOException, InterruptedException {
	
		BufferedReader br1 = new BufferedReader(new FileReader("Output\\Text1.txt"));
		BufferedReader br2 = new BufferedReader(new FileReader("Output\\Text2.txt"));
	
		try (br1; br2) {
			System.out.println(br1.readLine() + br2.readLine());
		}
		br1.close();
		br2.close();
	}
}
