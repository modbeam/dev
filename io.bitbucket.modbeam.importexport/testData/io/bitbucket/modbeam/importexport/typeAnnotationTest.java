package io.bitbucket.modbeam.importexport;

import java.lang.annotation. * ;@Target(ElementType.TYPE_USE)@interface TypeAnnoDemo {}

@SuppressWarnings("unused")
public class typeAnnotationTest {
	public static void main(String[] args) {@TypeAnnoDemo String s = "Hello,I am annotated with a type annotation";
    System.out.println(s);
    myMethod();
  }
	
  static@TypeAnnoDemo int myMethod() {
    System.out.println("There is a use of annotation with the return type of the function");
            return 0;
      }
}

