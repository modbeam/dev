package io.bitbucket.modbeam.importexport;

public class recordClassTest {

	public record Point(int x, int y) { }
	
	public static void main(String[] args) {
		Point test = new Point(1, 2);
		Point test2 = new Point(3, 4);
		
		System.out.println(test.x());
		System.out.println(test.y());
		
		System.out.println(test.toString());
		System.out.println(test.equals(test2));
		System.out.println(test.hashCode());
		System.out.println(test2.hashCode());
	}
}
