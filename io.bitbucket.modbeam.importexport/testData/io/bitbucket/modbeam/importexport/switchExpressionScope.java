package io.bitbucket.modbeam.importexport;

public class switchExpressionScope {

	public static void main(String[] args) {
		int k = 2;
		
		String s = switch (k) {
		case 1 -> {
			String temp = "one";
			yield temp;
		}
		case 2 -> {
			String temp = "two";
			yield temp;
		}
		default -> "more";
		};
		
		System.out.println(s);
	}
}
