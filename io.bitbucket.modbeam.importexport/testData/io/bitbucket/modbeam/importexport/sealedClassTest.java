package io.bitbucket.modbeam.importexport;

public class sealedClassTest {

	public abstract sealed class Vehicle permits Car {

	    protected final String registrationNumber;

	    public Vehicle(String registrationNumber) {
	        this.registrationNumber = registrationNumber;
	    }

	    public String getRegistrationNumber() {
	        return registrationNumber;
	    }

	}
	
	public non-sealed class Car extends Vehicle {

		public Car(String registrationNumber) {
			super(registrationNumber);
		}
    }

}
	

