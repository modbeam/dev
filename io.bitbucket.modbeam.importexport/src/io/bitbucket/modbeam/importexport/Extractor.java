package io.bitbucket.modbeam.importexport;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringEscapeUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.FrameNode;
import org.objectweb.asm.tree.IincInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.LineNumberNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.LookupSwitchInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;
import org.objectweb.asm.tree.TryCatchBlockNode;
import org.objectweb.asm.tree.TypeAnnotationNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

import io.bitbucket.modbeam.jbc.Annotation;
import io.bitbucket.modbeam.jbc.ArrayTypeSignature;
import io.bitbucket.modbeam.jbc.BaseTypeEnum;
import io.bitbucket.modbeam.jbc.Basetype;
import io.bitbucket.modbeam.jbc.BooleanValue;
import io.bitbucket.modbeam.jbc.ByteValue;
import io.bitbucket.modbeam.jbc.CharValue;
import io.bitbucket.modbeam.jbc.ClassSignature;
import io.bitbucket.modbeam.jbc.ClassTypeSignature;
import io.bitbucket.modbeam.jbc.Clazz;
import io.bitbucket.modbeam.jbc.ConditionalEdge;
import io.bitbucket.modbeam.jbc.ControlFlowEdge;
import io.bitbucket.modbeam.jbc.DoubleValue;
import io.bitbucket.modbeam.jbc.ElementValue;
import io.bitbucket.modbeam.jbc.ElementValuePair;
import io.bitbucket.modbeam.jbc.ElementaryValue;
import io.bitbucket.modbeam.jbc.EnumValue;
import io.bitbucket.modbeam.jbc.ExceptionTableEntry;
import io.bitbucket.modbeam.jbc.ExceptionalEdge;
import io.bitbucket.modbeam.jbc.Field;
import io.bitbucket.modbeam.jbc.FieldInstruction;
import io.bitbucket.modbeam.jbc.FieldReference;
import io.bitbucket.modbeam.jbc.FieldTypeSignature;
import io.bitbucket.modbeam.jbc.FloatValue;
import io.bitbucket.modbeam.jbc.FormalTypeParameter;
import io.bitbucket.modbeam.jbc.IincInstruction;
import io.bitbucket.modbeam.jbc.IndicatorEnum;
import io.bitbucket.modbeam.jbc.Instruction;
import io.bitbucket.modbeam.jbc.IntInstruction;
import io.bitbucket.modbeam.jbc.IntValue;
import io.bitbucket.modbeam.jbc.JbcFactory;
import io.bitbucket.modbeam.jbc.LdcDoubleInstruction;
import io.bitbucket.modbeam.jbc.LdcFloatInstruction;
import io.bitbucket.modbeam.jbc.LdcIntInstruction;
import io.bitbucket.modbeam.jbc.LdcLongInstruction;
import io.bitbucket.modbeam.jbc.LdcStringInstruction;
import io.bitbucket.modbeam.jbc.LdcTypeInstruction;
import io.bitbucket.modbeam.jbc.LocalVariableTableEntry;
import io.bitbucket.modbeam.jbc.LongValue;
import io.bitbucket.modbeam.jbc.Method;
import io.bitbucket.modbeam.jbc.MethodDescriptor;
import io.bitbucket.modbeam.jbc.MethodInstruction;
import io.bitbucket.modbeam.jbc.MethodReference;
import io.bitbucket.modbeam.jbc.MethodSignature;
import io.bitbucket.modbeam.jbc.MultianewarrayInstruction;
import io.bitbucket.modbeam.jbc.ReturnType;
import io.bitbucket.modbeam.jbc.ShortValue;
import io.bitbucket.modbeam.jbc.SimpleClassTypeSignature;
import io.bitbucket.modbeam.jbc.StringValue;
import io.bitbucket.modbeam.jbc.SwitchCaseEdge;
import io.bitbucket.modbeam.jbc.SwitchDefaultEdge;
import io.bitbucket.modbeam.jbc.ThrowsSignature;
import io.bitbucket.modbeam.jbc.TypeArgument;
import io.bitbucket.modbeam.jbc.TypeConcrete;
import io.bitbucket.modbeam.jbc.TypeInstruction;
import io.bitbucket.modbeam.jbc.TypeReference;
import io.bitbucket.modbeam.jbc.TypeSignature;
import io.bitbucket.modbeam.jbc.TypeVariableSignature;
import io.bitbucket.modbeam.jbc.TypeWild;
import io.bitbucket.modbeam.jbc.UnconditionalEdge;
import io.bitbucket.modbeam.jbc.VarInstruction;

public class Extractor {

    private final JbcFactory JBCMM_FACRTORY = JbcFactory.eINSTANCE;
    private int pos;
    private int currentPos;
    private Boolean readInterfaceBounds;
    private int dimension;

    public synchronized Clazz extract(InputStream bytecode) throws IOException {
        ClassReader cr = new ClassReader(bytecode);
        ClassNode classNode = new ClassNode();
        cr.accept(classNode, 0);
        return createClazz(classNode);
    }

    // private Project projectModel;
    private Map<String, Clazz> classModels = new HashMap<String, Clazz>();
    private Map<String, Method> methodModels = new HashMap<String, Method>();
    private Map<String, Field> fieldModels = new HashMap<String, Field>();

    /*private Project findOrCreateProjectModel() {
    	if (projectModel != null)
    		return projectModel;
    	
    	projectModel = JBCMM_FACRTORY.createProject();
    	return projectModel;
    }*/

    private Clazz findOrCreateClassModel(String internalClassname) {
        if (classModels.containsKey(internalClassname))
            return classModels.get(internalClassname);

        Clazz result = JBCMM_FACRTORY.createClazz();
        result.setName(internalClassname);
        classModels.put(internalClassname, result);
        return result;
    }

    private Method findOrCreateMethodModel(String declaringClass, String methodName, String methodDescriptor) {
        String key = declaringClass + " " + methodName + " " + methodDescriptor;
        if (methodModels.containsKey(key))
            return methodModels.get(key);
        Method result = JBCMM_FACRTORY.createMethod();

        result.setName(methodName);
        result.setDescriptor(createMethodDescriptor(methodDescriptor));

        methodModels.put(key, result);
        return result;
    }

    private Field findOrCreateFieldModel(String declaringClass, String fieldName, String fieldDescriptor) {
        String key = declaringClass + " " + fieldName + " " + fieldDescriptor;
        if (methodModels.containsKey(key))
            return fieldModels.get(key);

        Field result = JBCMM_FACRTORY.createField();

        result.setName(fieldName);
        result.setDescriptor(createTypeReference(fieldDescriptor));

        fieldModels.put(key, result);
        return result;
    }

    /**
     * 
     * @param typeDescriptor
     *            a type descriptor following the FieldDescriptor syntax plus V for
     *            void
     * @return
     */
    private TypeReference createTypeReference(String typeDescriptor) {
        TypeReference result = JBCMM_FACRTORY.createTypeReference();
        result.setTypeDescriptor(typeDescriptor);

        return result;
    }

    private MethodDescriptor createMethodDescriptor(String methodDescriptor) {
        MethodDescriptor result = JBCMM_FACRTORY.createMethodDescriptor();
        Type[] parameterTypes = Type.getArgumentTypes(methodDescriptor);
        Type resultType = Type.getReturnType(methodDescriptor);

        for (Type type : parameterTypes) {
            result.getParameterTypes().add(createTypeReference(type.getDescriptor()));
        }
        result.setResultType(createTypeReference(resultType.getDescriptor()));
        return result;
    }

    private MethodReference createMethodReference(String declaringClassDescriptor, String methodName,
            String methodDescriptor) {
        MethodReference result = JBCMM_FACRTORY.createMethodReference();
        result.setDeclaringClass(createTypeReference(Type.getObjectType(declaringClassDescriptor).getDescriptor()));
        result.setDescriptor(createMethodDescriptor(methodDescriptor));
        result.setName(methodName);
        return result;
    }

    private FieldReference createFieldReference(String declaringClassDescriptor, String fieldName,
            String fieldDescriptor) {
        FieldReference result = JBCMM_FACRTORY.createFieldReference();
        result.setDeclaringClass(createTypeReference(Type.getObjectType(declaringClassDescriptor).getDescriptor()));
        result.setName(fieldName);
        result.setDescriptor(createTypeReference(fieldDescriptor));
        return result;
    }

    private Clazz createClazz(ClassNode classNode) {
        Clazz result = findOrCreateClassModel(classNode.name);

        result.setSuperClass(createTypeReference(Type.getObjectType(classNode.superName).getDescriptor()));

        for (String interfaceName : (List<String>) classNode.interfaces) {
            result.getInterfaces().add(createTypeReference(Type.getObjectType(interfaceName).getDescriptor()));
        }

        result.setMinorVersion(classNode.version >> 16);
        result.setMajorVersion(classNode.version & 0xFF);

        result.setAbstract((classNode.access & Opcodes.ACC_ABSTRACT) != 0);
        result.setAnnotation((classNode.access & Opcodes.ACC_ANNOTATION) != 0);
        result.setDeprecated((classNode.access & Opcodes.ACC_DEPRECATED) != 0);
        result.setEnum((classNode.access & Opcodes.ACC_ENUM) != 0);
        result.setFinal((classNode.access & Opcodes.ACC_FINAL) != 0);
        result.setInterface((classNode.access & Opcodes.ACC_INTERFACE) != 0);
        result.setPrivate((classNode.access & Opcodes.ACC_PRIVATE) != 0);
        result.setProtected((classNode.access & Opcodes.ACC_PROTECTED) != 0);
        result.setPublic((classNode.access & Opcodes.ACC_PUBLIC) != 0);
        result.setSuper((classNode.access & Opcodes.ACC_SUPER) != 0);
        result.setSynthetic((classNode.access & Opcodes.ACC_SYNTHETIC) != 0);

        // TODO: currently ignored
        // for (InnerClassNode innerClass : (List<InnerClassNode>)
        // classNode.innerClasses) {
        // }

        if (classNode.outerClass != null)
            result.setOuterClass(createTypeReference(Type.getObjectType(classNode.outerClass).getDescriptor()));

        if (classNode.outerMethod != null) {
            result.setEnclosingMethod(
                    createMethodReference(classNode.outerClass, classNode.outerMethod, classNode.outerMethodDesc));
        }

        /*
         * Class signature handling by Wladimir Koenig
         */
        if (classNode.signature != null) {
            ClassSignature classSig = JBCMM_FACRTORY.createClassSignature();
            List<ClassTypeSignature> superInterfaces = new ArrayList<>();
            pos = 0;
            currentPos = 0;
            String signature = classNode.signature;

            List<FormalTypeParameter> formalTypeParameters = parseFormalTypeParameters(signature, pos);
            classSig.getFormalTypeParameter().addAll(formalTypeParameters);
            pos = currentPos;

            ClassTypeSignature superClass = parseForClassTypeSignature(signature, pos);
            classSig.setSuperClassSignature(superClass);
            pos = currentPos;

            while (pos < signature.length()) {
                superInterfaces.add(parseForClassTypeSignature(signature, pos));
                pos = currentPos;
            }
            classSig.getSuperInterfaceSignature().addAll(superInterfaces);
            result.setSignature(classSig);
        }

        if (classNode.invisibleAnnotations != null) {
            for (AnnotationNode annotationNode : (List<AnnotationNode>) classNode.invisibleAnnotations) {
                result.getRuntimeInvisibleAnnotations().add(createAnnotation(annotationNode));
            }
        }
        if (classNode.invisibleTypeAnnotations != null) {
            // TODO currently ignored
        }

        if (classNode.visibleAnnotations != null) {
            for (AnnotationNode annotationNode : (List<AnnotationNode>) classNode.visibleAnnotations) {
                result.getRuntimeVisibleAnnotations().add(createAnnotation(annotationNode));
            }
        }
        if (classNode.visibleTypeAnnotations != null) {
            // TODO currently ignored
        }

        if (classNode.sourceDebug != null) {
            result.setSourceDebugExtension(classNode.sourceDebug);
        }

        if (classNode.sourceFile != null) {
            result.setSourceFileName(classNode.sourceFile);
        }

        for (FieldNode fieldNode : (List<FieldNode>) classNode.fields) {
            result.getFields().add(createField(classNode.name, fieldNode));
        }

        for (MethodNode methodNode : (List<MethodNode>) classNode.methods) {
            result.getMethods().add(createMethod(classNode.name, methodNode));
        }

        return result;
    }

    /*
     * Start der Parser Methoden f�r Signaturen von Wladimir k�nig Parser f�r
     * FormalTypeParameters
     */
    private List<FormalTypeParameter> parseFormalTypeParameters(String sign, int pos) {
        List<FormalTypeParameter> formalTypeParameterList = new ArrayList<>();

        /*
         * Signatur mit Start Symbol '<' deutet auf FormalTypeParameter hin.
         * Ansonsten gib leere formalTypeParameterList zur�ck.
         */
        if (sign.charAt(pos) == '<') {
            Boolean readClassBound = false;
            String identifier = "";
            readInterfaceBounds = false;
            dimension = 0;
            pos++;

            FormalTypeParameter formalParameter = JBCMM_FACRTORY.createFormalTypeParameter();

            while (pos < sign.length()) {
                char c = sign.charAt(pos);
                /*
                 * Bounds eines FormalTypeParameters
                 */
                if (readClassBound) {
                    if (c == 'L') {
                        List<ClassTypeSignature> classTypeInterfaces = new ArrayList<>();
                        readClassBound = false;

                        ClassTypeSignature classBoundSig = parseForClassTypeSignature(sign, pos);
                        pos = currentPos;

                        while (readInterfaceBounds) {
                            ClassTypeSignature classTypeInterface = parseForClassTypeSignature(sign, pos);
                            pos = currentPos;
                            readInterfaceBounds = false;
                            classTypeInterfaces.add(classTypeInterface);
                        }

                        formalParameter.setClassBound(classBoundSig);
                        formalParameter.getInterfaceBound().addAll(classTypeInterfaces);

                        formalTypeParameterList.add(formalParameter);
                        formalParameter = JBCMM_FACRTORY.createFormalTypeParameter();
                    } else {
                        pos++;
                    }
                }

                /*
                 * Neuer Identifier eines FormalTypeParameters bestimmt
                 */
                else if (pos < sign.length() - 1 && Character.isLetter(c) && sign.charAt(pos + 1) == ':'
                        || pos < sign.length() - 1 && Character.isDigit(c) && sign.charAt(pos + 1) == ':') {
                    readClassBound = true;
                    identifier += c;
                    formalParameter.setIdentifier(identifier);
                    identifier = "";
                    pos++;
                }

                /*
                 * Identifier String wird erweitert.
                 */
                else if (pos < sign.length() - 1 && Character.isLetter(c)
                        || pos < sign.length() - 1 && Character.isDigit(c)) {
                    identifier += c;
                    pos++;
                }

                /*
                 * Abschluss des FormalTypeParameter Strings mit Symbol '>'
                 */
                else if (c == '>') {
                    pos++;
                    break;
                }
            }
            currentPos = pos;
            return formalTypeParameterList;
        } else {
            currentPos = pos;
            return formalTypeParameterList;
        }
    }

    /*
     * Parser f�r ClassTypeSignature und SimpleClassTypeSignature
     */
    private ClassTypeSignature parseForClassTypeSignature(String sign, int pos) {
        Boolean LTest = true;
        Boolean recursive = false;
        String classTypeParameter = "";
        String classTypePack = "";

        ClassTypeSignature classType = JBCMM_FACRTORY.createClassTypeSignature();
        SimpleClassTypeSignature simpleClassType = JBCMM_FACRTORY.createSimpleClassTypeSignature();
        SimpleClassTypeSignature suffixTypeArgument;
        List<TypeArgument> simpleClassTypeArguments = new ArrayList<>();
        List<SimpleClassTypeSignature> classSuffixes = new ArrayList<>();

        while (pos < sign.length()) {
            char c = sign.charAt(pos);

            /*
             * Erstes 'L' wird �bersprungen
             */
            if (LTest) {
                LTest = false;
                pos++;
            }

            /*
             * Fall 2: Bound besitzt mindestens ein TypeArgument
             */
            else if (c == '<') {
                pos++;
                recursive = true;
                simpleClassType.setIdentifier(classTypeParameter);
                classTypeParameter = "";

                if (classTypePack != "") {
                    classTypePack = classTypePack.substring(0, classTypePack.length() - 1);
                    classType.setPackagespecifier(classTypePack);
                    classTypePack = "";
                }
                simpleClassType.setClassTypeSignature(classType);
                simpleClassTypeArguments = parseForTypeArguments(sign, pos);
                simpleClassType.getTypeArgument().addAll(simpleClassTypeArguments);
                classType.setSimpleClassSignature(simpleClassType);
                pos = currentPos;
            }

            /*
             * Fall 3: Bound gefolgt von mindestens einem ClassSuffix
             */
            else if (c == '.') {
                List<TypeArgument> suffixTypeArguments = new ArrayList<>();
                suffixTypeArgument = JBCMM_FACRTORY.createSimpleClassTypeSignature();
                pos++;
                c = sign.charAt(pos);
                String type = "";

                while (pos < sign.length()) {
                    c = sign.charAt(pos);

                    if (c == '<') {
                        pos++;
                        suffixTypeArgument.setIdentifier(type);
                        type = "";

                        suffixTypeArgument.setClassTypeSignatureSuffix(classType);
                        suffixTypeArguments = parseForTypeArguments(sign, pos);
                        suffixTypeArgument.getTypeArgument().addAll(suffixTypeArguments);
                        classSuffixes.add(suffixTypeArgument);
                        pos = currentPos;
                        break;
                    }
                    pos++;
                    type += c;
                }
            }

            /*
             * Fall 3: Bound gefolgt von mindestens einem InterfaceBound oder
             * einem weiteren FormalTypeParameter
             */
            else if (c == ';') {
                if (pos < sign.length() - 1 && sign.charAt(pos + 1) == ':') {
                    readInterfaceBounds = true;
                    pos += 2;
                    break;
                } else {
                    pos++;
                    break;
                }
            }

            /*
             * Fall 3: Paket '/' wird im String pack gespeichert
             */
            else if (c == '/') {
                classTypeParameter += c;
                classTypePack += classTypeParameter;
                classTypeParameter = "";
                pos++;
            }

            /*
             * Fall 4: String wird in classParameter gespeichert oder
             * Iteriervorgang wird abgeschlossen da String fertiggelesen wurde
             */
            else {
                if (pos < sign.length() - 1) {
                    classTypeParameter = classTypeParameter + c;
                    pos++;
                } else {
                    break;
                }
            }
        }

        if (!recursive) {
            simpleClassType.setIdentifier(classTypeParameter);
            classTypeParameter = "";

            if (classTypePack != "") {
                classTypePack = classTypePack.substring(0, classTypePack.length() - 1);
                classType.setPackagespecifier(classTypePack);
                classTypePack = "";
            }
            simpleClassType.setClassTypeSignature(classType);
            simpleClassType.getTypeArgument().addAll(simpleClassTypeArguments);

            classType.setSimpleClassSignature(simpleClassType);
            for (SimpleClassTypeSignature scts : classSuffixes) {
                classType.setSimpleClassSignature(scts);
            }
            classType.getSimpleClassSignatureSuffix().addAll(classSuffixes);
        }
        currentPos = pos;
        return classType;
    }

    /*
     * Parser f�r TypeArguments
     */
    private List<TypeArgument> parseForTypeArguments(String sign, int pos) {
        Boolean indicatorPlus = false;
        Boolean indicatorMinus = false;
        TypeWild wildType;
        TypeConcrete concreteType;
        TypeVariableSignature variableType;
        ClassTypeSignature argumentClassType;
        List<TypeArgument> simpleClassTypeArgumentList = new ArrayList<>();

        while (pos < sign.length()) {
            char c = sign.charAt(pos);

            /*
             * Fall 1: Wildcard Indicator + vor einem TypeArgument
             */
            if (c == '+') {
                indicatorPlus = true;
                pos++;
            }

            /*
             * Fall 2: Wildcard Indicator + vor einem TypeArgument
             */
            else if (c == '-') {
                indicatorMinus = true;
                pos++;
            }

            /*
             * Fall 3: TypeArgument vom Typ WildType
             */
            else if (c == '*') {
                wildType = JBCMM_FACRTORY.createTypeWild();
                simpleClassTypeArgumentList.add(wildType);
                pos++;
            }

            /*
             * Fall 4: TypeArgument vom Typ ArrayType
             */
            else if (c == '[') {
                concreteType = JBCMM_FACRTORY.createTypeConcrete();

                if (indicatorPlus) {
                    concreteType.setWildcardIndicator(IndicatorEnum.SUB);
                    indicatorPlus = false;
                } else if (indicatorMinus) {
                    concreteType.setWildcardIndicator(IndicatorEnum.SUPER);
                    indicatorMinus = false;
                } else {
                    concreteType.setWildcardIndicator(IndicatorEnum.EXACT);
                }
                dimension += 1;
                pos++;
                concreteType = parseForArrayTypeSignature(sign, pos);
                pos = currentPos;
                simpleClassTypeArgumentList.add(concreteType);
            }

            /*
             * Fall 5: TypeArgument vom Typ VariableType
             */
            else if (c == 'T') {
                concreteType = JBCMM_FACRTORY.createTypeConcrete();
                variableType = JBCMM_FACRTORY.createTypeVariableSignature();
                String type = "";
                pos++;
                c = sign.charAt(pos);

                while (c != ';') {
                    type += c;
                    pos++;
                    c = sign.charAt(pos);
                }
                variableType.setIdentifier(type);

                if (indicatorPlus) {
                    concreteType.setWildcardIndicator(IndicatorEnum.SUB);
                    indicatorPlus = false;
                } else if (indicatorMinus) {
                    concreteType.setWildcardIndicator(IndicatorEnum.SUPER);
                    indicatorMinus = false;
                } else {
                    concreteType.setWildcardIndicator(IndicatorEnum.EXACT);
                }
                concreteType.setFieldTypeSignature(variableType);
                simpleClassTypeArgumentList.add(concreteType);
                pos++;
            }

            /*
             * Fall 6: TypeArgument vom Typ ClassType
             */
            else if (c == 'L') {
                concreteType = JBCMM_FACRTORY.createTypeConcrete();
                argumentClassType = parseForClassTypeSignature(sign, pos);
                pos = currentPos;

                if (indicatorPlus) {
                    concreteType.setWildcardIndicator(IndicatorEnum.SUB);
                    indicatorPlus = false;
                } else if (indicatorMinus) {
                    concreteType.setWildcardIndicator(IndicatorEnum.SUPER);
                    indicatorMinus = false;
                } else {
                    concreteType.setWildcardIndicator(IndicatorEnum.EXACT);
                }

                concreteType.setFieldTypeSignature(argumentClassType);
                simpleClassTypeArgumentList.add(concreteType);
            }

            /*
             * Fall 7: alle TypeArguments eingelesen
             */
            else if (c == '>') {
                pos++;
                break;
            }
        }
        currentPos = pos;
        return simpleClassTypeArgumentList;
    }

    /*
     * Parser f�r ArrayType TypeArguments
     */
    private TypeConcrete parseForArrayTypeSignature(String sign, int pos) {

        TypeConcrete concreteType = JBCMM_FACRTORY.createTypeConcrete();
        ClassTypeSignature classType;
        TypeVariableSignature variableType;
        ArrayTypeSignature arrayType;

        while (pos < sign.length()) {
            char c = sign.charAt(pos);

            if (c == '[') {
                dimension += 1;
                pos++;
            }

            /*
             * Fall 2: Array mit BaseType
             */
            else if (c == 'B' || c == 'C' || c == 'D' || c == 'F' || c == 'I' || c == 'J' || c == 'S' || c == 'Z') {

                Basetype baseType = JBCMM_FACRTORY.createBasetype();
                arrayType = JBCMM_FACRTORY.createArrayTypeSignature();

                switch (c) {
                case 'B':
                    baseType.setType(BaseTypeEnum.BYTE);
                    break;
                case 'C':
                    baseType.setType(BaseTypeEnum.CHAR);
                    break;
                case 'D':
                    baseType.setType(BaseTypeEnum.DOUBLE);
                    break;
                case 'F':
                    baseType.setType(BaseTypeEnum.FLOAT);
                    break;
                case 'I':
                    baseType.setType(BaseTypeEnum.INTEGER);
                    break;
                case 'J':
                    baseType.setType(BaseTypeEnum.LONG);
                    break;
                case 'S':
                    baseType.setType(BaseTypeEnum.SHORT);
                    break;
                case 'Z':
                    baseType.setType(BaseTypeEnum.BOOLEAN);
                    break;
                }
                arrayType.setDimension(dimension);
                arrayType.setTypeSignature(baseType);
                concreteType.setFieldTypeSignature(arrayType);
                dimension = 0;
                pos++;
                break;
            }

            /*
             * Fall 3: Array mit VariableType
             */
            else if (c == 'T') {
                variableType = JBCMM_FACRTORY.createTypeVariableSignature();
                concreteType = JBCMM_FACRTORY.createTypeConcrete();
                arrayType = JBCMM_FACRTORY.createArrayTypeSignature();
                String type = "";
                pos++;
                c = sign.charAt(pos);

                while (c != ';') {
                    type += c;
                    pos++;
                    c = sign.charAt(pos);
                }
                variableType.setIdentifier(type);

                arrayType.setDimension(dimension);
                arrayType.setTypeSignature(variableType);
                concreteType.setFieldTypeSignature(arrayType);
                dimension = 0;
                pos++;
                break;
            }

            /*
             * Rekursiv! Fall 4: Array mit ClassType
             */
            else {
                concreteType = JBCMM_FACRTORY.createTypeConcrete();
                arrayType = JBCMM_FACRTORY.createArrayTypeSignature();

                pos++;
                classType = parseForClassTypeSignature(sign, pos);
                pos = currentPos;
                arrayType.setDimension(dimension);
                arrayType.setTypeSignature(classType);
                concreteType.setFieldTypeSignature(arrayType);
                dimension = 0;
                break;
            }
        }
        currentPos = pos;
        return concreteType;
    }

    /*
     * Parser f�r Feld Signaturen
     */
    private FieldTypeSignature parseForFieldTypeSignature(String sign, int pos) {
        char c = sign.charAt(pos);
        ClassTypeSignature sigClass;
        TypeVariableSignature sigVar;
        ArrayTypeSignature sigArr;
        FieldTypeSignature sig = null;

        /*
         * Feld ist ein Object
         */
        if (c == 'L') {
            sigClass = parseForClassTypeSignature(sign, pos);
            currentPos = pos;
            sig = sigClass;

            /*
             * Feld ist eine generischer Typ
             */
        } else if (c == 'T') {
            String type = "";
            pos++;
            c = sign.charAt(pos);
            while (c != ';') {
                type += c;
                pos++;
                c = sign.charAt(pos);
            }
            sigVar = JBCMM_FACRTORY.createTypeVariableSignature();
            sigVar.setIdentifier(type);
            sig = sigVar;

            /*
             * Feld ist ein Array
             */
        } else if (c == '[') {
            dimension = 0;

            while (c == '[') {
                dimension += 1;
                pos++;
                c = sign.charAt(pos);
            }
            sigArr = JBCMM_FACRTORY.createArrayTypeSignature();
            sigArr.setDimension(dimension);
            sigArr.setTypeSignature(parseForTypeSignature(sign, pos));
            pos = currentPos;
            sig = sigArr;
        }
        return sig;
    }

    /*
     * Parser f�r Methoden Signaturen
     */
    private ReturnType parseForReturnType(String sign, int pos) {
        ReturnType returnType;
        char c = sign.charAt(pos);

        /*
         * Fall 1 R�ckgabe void
         */
        if (c == 'V') {
            returnType = JBCMM_FACRTORY.createVoidDescriptor();

            /*
             * Fall 2 R�ckgabe ein TypeSignature
             */
        } else {
            returnType = parseForTypeSignature(sign, pos);
        }
        return returnType;
    }

    TypeSignature parseForTypeSignature(String sign, int pos) {
        char c = sign.charAt(pos);

        if (c == 'B' || c == 'C' || c == 'D' || c == 'F' || c == 'I' || c == 'J' || c == 'S' || c == 'Z') {
            Basetype baseType = JBCMM_FACRTORY.createBasetype();

            switch (c) {
            case 'B':
                baseType.setType(BaseTypeEnum.BYTE);
                break;
            case 'C':
                baseType.setType(BaseTypeEnum.CHAR);
                break;
            case 'D':
                baseType.setType(BaseTypeEnum.DOUBLE);
                break;
            case 'F':
                baseType.setType(BaseTypeEnum.FLOAT);
                break;
            case 'I':
                baseType.setType(BaseTypeEnum.INTEGER);
                break;
            case 'J':
                baseType.setType(BaseTypeEnum.LONG);
                break;
            case 'S':
                baseType.setType(BaseTypeEnum.SHORT);
                break;
            case 'Z':
                baseType.setType(BaseTypeEnum.BOOLEAN);
                break;
            }
            pos++;
            currentPos = pos;
            return baseType;

            /*
             * Fall 3 R�ckgabe Generischer Typ
             */
        } else if (c == 'T') {
            TypeVariableSignature varType = JBCMM_FACRTORY.createTypeVariableSignature();
            String type = "";
            pos++;
            c = sign.charAt(pos);

            while (c != ';') {
                type += c;
                pos++;
                c = sign.charAt(pos);
            }
            varType.setIdentifier(type);

            pos++;
            currentPos = pos;
            return varType;

            /*
             * Fall 4 R�ckgabe Objekt
             */
        } else if (c == 'L') {
            ClassTypeSignature classType = parseForClassTypeSignature(sign, pos);
            return classType;

            /*
             * Fall 5 R�ckgabe Array
             */
        } else if (c == '[') {
            dimension = 0;
            ArrayTypeSignature sigArr = JBCMM_FACRTORY.createArrayTypeSignature();

            while (c == '[') {
                dimension += 1;
                pos++;
                c = sign.charAt(pos);
            }
            sigArr.setDimension(dimension);
            sigArr.setTypeSignature(parseForTypeSignature(sign, pos));
            pos = currentPos;
            return sigArr;
        } 
        
        return null;
    }

    /*
     * 
     * Parsermethoden f�r Signaturen von Wladimir k�nig FERTIG!!!
     * 
     */
    
    
    /*
     * Parsermethode f�r Throws Signaturen
     */
    private List<ThrowsSignature> parseForThrowsSignature(String sign, int pos) {
    	
    	List<ThrowsSignature> result = new ArrayList<>();
    	
    	for(String type: sign.substring(pos+1).split("\\^")) {
    		ThrowsSignature throwSig = (ThrowsSignature) parseForTypeSignature(type, 0);
    		result.add(throwSig);
    	}
    	return result;
    }

    private Annotation createAnnotation(AnnotationNode annotationNode) {
        Annotation result = JBCMM_FACRTORY.createAnnotation();
        result.setType(createTypeReference(annotationNode.desc));
        if (annotationNode.values != null) {
            Iterator<?> it = ((List<?>) annotationNode.values).iterator();
            while (it.hasNext()) {
                String attributeName = (String) it.next();
                Object attributeValue = it.next();

                ElementValuePair elementValuePair = JBCMM_FACRTORY.createElementValuePair();
                elementValuePair.setName(attributeName);
                elementValuePair.setValue(createElementValue(attributeValue));

                result.getElementValuePairs().add(elementValuePair);
            }
        }
        return result;
    }

    private ElementValue createElementValue(Object attributeValue) {
        ElementValue result = JBCMM_FACRTORY.createElementValue();
        if (attributeValue instanceof AnnotationNode) {
            result.setAnnotationValue(createAnnotation((AnnotationNode) attributeValue));
        } else if (attributeValue instanceof Type) {
            result.setClassValue(createTypeReference(((Type) attributeValue).getDescriptor()));
        } else if (attributeValue instanceof String[]) {
            String enumTypeName = ((String[]) attributeValue)[0];
            String enumConstantName = ((String[]) attributeValue)[1];
            result.setEnumValue(createEnumValue(enumTypeName, enumConstantName));
        } else if (attributeValue instanceof List) {
            for (Object listElementValue : (List<?>) attributeValue) {
                result.getArrayValue().add(createElementValue(listElementValue));
            }
        } else {
            result.setConstantValue(createElementaryValue(attributeValue));
        }
        return result;
    }

    private ElementaryValue createElementaryValue(Object attributeValue) {
        if (attributeValue instanceof Boolean) {
            BooleanValue result = JBCMM_FACRTORY.createBooleanValue();
            result.setValue((Boolean) attributeValue);
            return result;
        } else if (attributeValue instanceof Character) {
            String unescaped = ((Character) attributeValue).toString();
            String escaped;
            if (unescaped.equals("\'"))
                escaped = "\\\'";
            else
                escaped = StringEscapeUtils.escapeJava(unescaped);

            CharValue result = JBCMM_FACRTORY.createCharValue();
            result.setValue(escaped);
            return result;
        } else if (attributeValue instanceof Byte) {
            ByteValue result = JBCMM_FACRTORY.createByteValue();
            result.setValue((Byte) attributeValue);
            return result;
        } else if (attributeValue instanceof Short) {
            ShortValue result = JBCMM_FACRTORY.createShortValue();
            result.setValue((Short) attributeValue);
            return result;
        } else if (attributeValue instanceof Integer) {
            IntValue result = JBCMM_FACRTORY.createIntValue();
            result.setValue((Integer) attributeValue);
            return result;
        } else if (attributeValue instanceof Long) {
            LongValue result = JBCMM_FACRTORY.createLongValue();
            result.setValue((Long) attributeValue);
            return result;
        } else if (attributeValue instanceof Float) {
            FloatValue result = JBCMM_FACRTORY.createFloatValue();
            result.setValue((Float) attributeValue);
            return result;
        } else if (attributeValue instanceof Double) {
            DoubleValue result = JBCMM_FACRTORY.createDoubleValue();
            result.setValue((Double) attributeValue);
            return result;
        } else if (attributeValue instanceof String) {
            StringValue result = JBCMM_FACRTORY.createStringValue();
            result.setValue(StringEscapeUtils.escapeJava((String) attributeValue));
            return result;
        } else {
            throw new RuntimeException("Unsupported type of ElementaryValue (class: " + attributeValue.getClass()
                    + ", value: " + attributeValue + ").");
        }
    }

    private EnumValue createEnumValue(String enumTypeName, String enumConstantName) {
        EnumValue result = JBCMM_FACRTORY.createEnumValue();
        result.setType(createTypeReference(enumTypeName));
        result.setConstName(enumConstantName);
        return result;
    }

    private Field createField(String declaringClassName, FieldNode fieldNode) {
        Field result = findOrCreateFieldModel(declaringClassName, fieldNode.name, fieldNode.desc);

        result.setPrivate((fieldNode.access & Opcodes.ACC_PRIVATE) != 0);
        result.setProtected((fieldNode.access & Opcodes.ACC_PROTECTED) != 0);
        result.setPublic((fieldNode.access & Opcodes.ACC_PUBLIC) != 0);
        result.setStatic((fieldNode.access & Opcodes.ACC_STATIC) != 0);
        result.setFinal((fieldNode.access & Opcodes.ACC_FINAL) != 0);
        result.setVolatile((fieldNode.access & Opcodes.ACC_VOLATILE) != 0);
        result.setTransient((fieldNode.access & Opcodes.ACC_TRANSIENT) != 0);
        result.setSynthetic((fieldNode.access & Opcodes.ACC_SYNTHETIC) != 0);
        result.setEnum((fieldNode.access & Opcodes.ACC_ENUM) != 0);

        /*
         * Feldsignaturen Implementierung von Wladimir K�nig
         */
        if (fieldNode.signature != null) {
            pos = 0;
            currentPos = 0;
            String sign = fieldNode.signature;

            FieldTypeSignature sig = parseForFieldTypeSignature(sign, pos);
            result.setSignature(sig);
        }

        if (fieldNode.invisibleAnnotations != null) {
        	System.out.println("F INVISIBLE");
            for (AnnotationNode annotationNode : (List<AnnotationNode>) fieldNode.invisibleAnnotations) {
                result.getRuntimeInvisibleAnnotations().add(createAnnotation(annotationNode));
            }
        }
        if (fieldNode.invisibleTypeAnnotations != null) {
        	System.out.println("F INVISIBLE T");
            // TODO currently ignored
        }

        if (fieldNode.visibleAnnotations != null) {
        	System.out.println("F VISIBLE");
            for (AnnotationNode annotationNode : (List<AnnotationNode>) fieldNode.visibleAnnotations) {
                result.getRuntimeVisibleAnnotations().add(createAnnotation(annotationNode));
            }
        }
        if (fieldNode.visibleTypeAnnotations != null) {
        	System.out.println("F VISIBLE TYPE");
            // TODO currently ignored
        }

        if (fieldNode.value != null) {
            result.setConstantValue(createElementaryValue(fieldNode.value));
        }
        return result;
    }
    
    private Method createMethod(String ownerTypeDescriptor, MethodNode methodNode) {
        // a mapping from ASM instruction to ECore instruction model
        Map<AbstractInsnNode, Instruction> insn2Model = new HashMap<AbstractInsnNode, Instruction>();
        List<ControlFlowEdge> edges = new ArrayList<ControlFlowEdge>();
        Method result = findOrCreateMethodModel(ownerTypeDescriptor, methodNode.name, methodNode.desc);

        result.setAbstract((methodNode.access & Opcodes.ACC_ABSTRACT) != 0);
        result.setBridge((methodNode.access & Opcodes.ACC_BRIDGE) != 0);
        result.setDeprecated((methodNode.access & Opcodes.ACC_DEPRECATED) != 0);
        result.setFinal((methodNode.access & Opcodes.ACC_FINAL) != 0);
        result.setNative((methodNode.access & Opcodes.ACC_NATIVE) != 0);
        result.setPrivate((methodNode.access & Opcodes.ACC_PRIVATE) != 0);
        result.setProtected((methodNode.access & Opcodes.ACC_PROTECTED) != 0);
        result.setPublic((methodNode.access & Opcodes.ACC_PUBLIC) != 0);
        result.setStatic((methodNode.access & Opcodes.ACC_STATIC) != 0);
        result.setStrict((methodNode.access & Opcodes.ACC_STRICT) != 0);
        result.setSynchronized((methodNode.access & Opcodes.ACC_SYNCHRONIZED) != 0);
        result.setSynthetic((methodNode.access & Opcodes.ACC_SYNTHETIC) != 0);
        result.setVarArgs((methodNode.access & Opcodes.ACC_VARARGS) != 0);
           
        // Exceptions can be thrown
        List<TypeReference> exceptions = new ArrayList<>();
        
        for(String ex: methodNode.exceptions) {
        	exceptions.add(createTypeReference("L"+ex));
        }
        
        result.getExceptionsCanBeThrown().addAll(exceptions);
        //   
        
        /*
         * Methoden Signatur Implementierung von Wladimir K�nig
         */
        if (methodNode.signature != null) {
            pos = 0;
            currentPos = 0;
            String sign = methodNode.signature;
                   
            char c = sign.charAt(pos);
            MethodSignature Methodsig = JBCMM_FACRTORY.createMethodSignature();

            List<FormalTypeParameter> formalTypeParameters = parseFormalTypeParameters(sign, pos);
            Methodsig.getFormalTypeParameter().addAll(formalTypeParameters);
            pos = currentPos + 1;


            List<TypeSignature> types = new ArrayList<>();
            while (c != ')' &&  !sign.contains("()")) {
                c = sign.charAt(pos);

                TypeSignature type = parseForTypeSignature(sign, pos);
                pos = currentPos;
                c = sign.charAt(pos);                
                types.add(type);
            }        
            Methodsig.getTypeSignature().addAll(types);
            pos++;

            ReturnType returnType = parseForReturnType(sign, pos);
            Methodsig.setReturnType(returnType);
            pos++;
            
            // ThrowsSignature
            if(pos<sign.length() && sign.charAt(pos)=='^') {
            	List<ThrowsSignature> throwsSignatures = parseForThrowsSignature(sign, pos);
            	Methodsig.getThrowsSignature().addAll(throwsSignatures);
            	}
            
            // Wurde bisher nicht gesetzt
            result.setSignature(Methodsig);
        }

        if (methodNode.invisibleAnnotations != null) {
            for (AnnotationNode annotationNode : (List<AnnotationNode>) methodNode.invisibleAnnotations) {
                result.getRuntimeInvisibleAnnotations().add(createAnnotation(annotationNode));
            }
        }
        if (methodNode.invisibleTypeAnnotations != null) {
            // TODO currently ignored
        }

        if (methodNode.visibleAnnotations != null) {
            for (AnnotationNode annotationNode : (List<AnnotationNode>) methodNode.visibleAnnotations) {
                result.getRuntimeVisibleAnnotations().add(createAnnotation(annotationNode));
            }
        }
        if (methodNode.visibleTypeAnnotations != null) {
            // TODO currently ignored
        }
        
        List<ExceptionTableEntry> exceptionTableEntries = new ArrayList<ExceptionTableEntry>();

        Map<LabelNode, List<ExceptionTableEntry>> startLabelToTryCatchBlock = new HashMap<LabelNode, List<ExceptionTableEntry>>();
        Map<LabelNode, List<ExceptionTableEntry>> endLabelToTryCatchBlock = new HashMap<LabelNode, List<ExceptionTableEntry>>();
        Map<LabelNode, List<ExceptionTableEntry>> handlerLabelToTryCatchBlock = new HashMap<LabelNode, List<ExceptionTableEntry>>();

        int eteIndex = 0;
        for (TryCatchBlockNode tryCatchBlockNode : (List<TryCatchBlockNode>) methodNode.tryCatchBlocks) {
            ExceptionTableEntry exceptionTableEntry = JBCMM_FACRTORY.createExceptionTableEntry();
            exceptionTableEntries.add(exceptionTableEntry);
            exceptionTableEntry.setName("ete_" + eteIndex);
            eteIndex++;
            if (tryCatchBlockNode.type != null)
                exceptionTableEntry
                        .setCatchType(createTypeReference(Type.getObjectType(tryCatchBlockNode.type).getDescriptor()));
            // TODO currently not supported: type annotations
            List<ExceptionTableEntry> startEntries = startLabelToTryCatchBlock.get(tryCatchBlockNode.start);
            if (startEntries == null) {
                startEntries = new ArrayList<ExceptionTableEntry>();
                startLabelToTryCatchBlock.put(tryCatchBlockNode.start, startEntries);
            }
            startEntries.add(exceptionTableEntry);
            List<ExceptionTableEntry> endEntries = endLabelToTryCatchBlock.get(tryCatchBlockNode.end);
            if (endEntries == null) {
                endEntries = new ArrayList<ExceptionTableEntry>();
                endLabelToTryCatchBlock.put(tryCatchBlockNode.end, endEntries);
            }
            endEntries.add(exceptionTableEntry);
            List<ExceptionTableEntry> handlerEntries = handlerLabelToTryCatchBlock.get(tryCatchBlockNode.handler);
            if (handlerEntries == null) {
                handlerEntries = new ArrayList<ExceptionTableEntry>();
                handlerLabelToTryCatchBlock.put(tryCatchBlockNode.handler, handlerEntries);
            }
            handlerEntries.add(exceptionTableEntry);
        }

        result.getExceptionTable().addAll(exceptionTableEntries);

        SortedMap<Integer, Instruction> instructionOrder = new TreeMap<Integer, Instruction>();
        createInstructions(methodNode, insn2Model, edges, startLabelToTryCatchBlock, endLabelToTryCatchBlock,
                handlerLabelToTryCatchBlock, exceptionTableEntries, result, instructionOrder);

        List<Instruction> instructions = new ArrayList<Instruction>(instructionOrder.values());
        for (int i = 0; i < instructions.size(); i++)
            instructions.get(i).setName("idx_" + i);

        if (methodNode.instructions.getFirst() != null)
            result.setFirstInstruction(findOrCreateNextRealInstruction(insn2Model, methodNode.instructions.getFirst()));

        return result;
    }

    private void createInstructions(MethodNode methodNode, Map<AbstractInsnNode, Instruction> insn2Model,
            List<ControlFlowEdge> edges, Map<LabelNode, List<ExceptionTableEntry>> startLabelToTryCatchBlock,
            Map<LabelNode, List<ExceptionTableEntry>> endLabelToTryCatchBlock,
            Map<LabelNode, List<ExceptionTableEntry>> handlerLabelToTryCatchBlock,
            List<ExceptionTableEntry> exceptionTableEntries, Method methodModel,
            SortedMap<Integer, Instruction> instructionOrder) {
        // preparations to aggregate debug information (the local variable
        // names, line numbers)
        LocalVariableNode[] locals = new LocalVariableNode[methodNode.maxLocals];
        List<ExceptionTableEntry> activeExceptionTableEntries = new ArrayList<ExceptionTableEntry>();

        int currentLineNumber = -1;

        Map<LabelNode, List<LocalVariableNode>> startLabel2Local = new HashMap<LabelNode, List<LocalVariableNode>>();
        Map<LabelNode, List<LocalVariableNode>> endLabel2Local = new HashMap<LabelNode, List<LocalVariableNode>>();
        Map<LocalVariableNode, LocalVariableTableEntry> localVariableModels = new HashMap<LocalVariableNode, LocalVariableTableEntry>();

        if (methodNode.localVariables != null) {
            for (LocalVariableNode localNode : (List<LocalVariableNode>) methodNode.localVariables) {
                LocalVariableTableEntry localVariableModel = JBCMM_FACRTORY.createLocalVariableTableEntry();
                localVariableModel.setIndex(localNode.index);
                localVariableModel.setName(localNode.name);
                localVariableModel.setDescriptor(createTypeReference(localNode.desc));

                localVariableModels.put(localNode, localVariableModel);
                methodModel.getLocalVariableTable().add(localVariableModel);

                
                //support signature
                if (localNode.signature != null) {
                	localVariableModel.setSignature(parseForFieldTypeSignature(localNode.signature, 0));
                }
                //
                
                List<LocalVariableNode> startEntries = startLabel2Local.get(localNode.start);
                if (startEntries == null) {
                    startEntries = new ArrayList<LocalVariableNode>();
                    startLabel2Local.put(localNode.start, startEntries);
                }
                List<LocalVariableNode> endEntries = endLabel2Local.get(localNode.end);
                if (endEntries == null) {
                    endEntries = new ArrayList<LocalVariableNode>();
                    endLabel2Local.put(localNode.end, endEntries);
                }

                startEntries.add(localNode);
                endEntries.add(localNode);
            }
        }

        ArrayList<ExceptionalEdge> exceptionalEdges = new ArrayList<ExceptionalEdge>();
        InsnList insnList = methodNode.instructions;
        AbstractInsnNode currentInstruction = insnList.getFirst();
        Instruction previousInstructionModel = null;

        while (currentInstruction != null) {
            Instruction currentInstructionModel = null;
            // handle local variable debug information
            if (currentInstruction instanceof LabelNode) {
                // process locals
                LabelNode labelNode = (LabelNode) currentInstruction;
                if (endLabel2Local.containsKey(labelNode))
                    for (LocalVariableNode localNode : endLabel2Local.get(labelNode)) {
                        localVariableModels.get(locals[localNode.index]).setEndInstruction(previousInstructionModel);
                        ;
                        locals[localNode.index] = null;
                    }
                if (startLabel2Local.containsKey(labelNode))
                    for (LocalVariableNode localNode : startLabel2Local.get(labelNode)) {
                        locals[localNode.index] = localNode;
                        localVariableModels.get(locals[localNode.index])
                                .setStartInstruction(findOrCreateNextRealInstruction(insn2Model, labelNode));
                    }

                // process exception handling
                if (startLabelToTryCatchBlock.containsKey(labelNode)) {
                    for (ExceptionTableEntry exceptionTableEntry : startLabelToTryCatchBlock.get(labelNode)) {
                        Instruction startInstruction = findOrCreateNextRealInstruction(insn2Model, labelNode);
                        exceptionTableEntry.setStartInstruction(startInstruction);
                        activeExceptionTableEntries.add(exceptionTableEntry);
                    }
                }
                if (endLabelToTryCatchBlock.containsKey(labelNode)) {
                    for (ExceptionTableEntry exceptionTableEntry : endLabelToTryCatchBlock.get(labelNode)) {
                        Instruction endInstruction = findOrCreateNextRealInstruction(insn2Model, labelNode);
                        exceptionTableEntry.setEndInstruction(endInstruction);
                        activeExceptionTableEntries.remove(exceptionTableEntry);
                    }
                }
                if (handlerLabelToTryCatchBlock.containsKey(labelNode)) {
                    for (ExceptionTableEntry exceptionTableEntry : handlerLabelToTryCatchBlock.get(labelNode)) {
                        Instruction handlerInstruction = findOrCreateNextRealInstruction(insn2Model, labelNode);
                        exceptionTableEntry.setHandlerInstruction(handlerInstruction);
                    }
                }
            }
            // handle line number debug information
            else if (currentInstruction instanceof LineNumberNode) {
                currentLineNumber = ((LineNumberNode) currentInstruction).line;
            }
            // ignore frame information
            else if (currentInstruction instanceof FrameNode) {
            }
            // handle real instructions
            else {
                currentInstructionModel = findOrCreateInstruction(insn2Model, currentInstruction);
                currentInstructionModel.setMethod(methodModel);
                previousInstructionModel = currentInstructionModel;

                // general information
                currentInstructionModel.setLinenumber(currentLineNumber);
                instructionOrder.put(insnList.indexOf(currentInstruction), currentInstructionModel);

                if (currentInstruction instanceof JumpInsnNode) {
                    JumpInsnNode jumpInsn = (JumpInsnNode) currentInstruction;
                    Instruction targetInstructionModel = findOrCreateNextRealInstruction(insn2Model, jumpInsn.label);

                    if (currentInstruction.getOpcode() == Opcodes.GOTO
                            || currentInstruction.getOpcode() == Opcodes.JSR) {
                        ControlFlowEdge edge = JBCMM_FACRTORY.createUnconditionalEdge();
                        edge.setStart(currentInstructionModel);
                        edge.setEnd(targetInstructionModel);

                        currentInstructionModel.getOutEdges().add(edge);
                        // targetInstructionModel.getInEdges().add(edge);
                        edges.add(edge);

                        Instruction fallthroughInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                                currentInstruction.getNext());
                        ControlFlowEdge ftEdge = JBCMM_FACRTORY.createResumeEdge();
                        ftEdge.setStart(currentInstructionModel);
                        ftEdge.setEnd(fallthroughInstructionModel);

                        currentInstructionModel.getOutEdges().add(ftEdge);
                        // fallthroughInstructionModel.getInEdges().add(ftEdge);
                        edges.add(ftEdge);
                    } else {
                        ConditionalEdge trueEdge = JBCMM_FACRTORY.createConditionalEdge();
                        trueEdge.setCondition(true);
                        trueEdge.setStart(currentInstructionModel);
                        trueEdge.setEnd(targetInstructionModel);
                        currentInstructionModel.getOutEdges().add(trueEdge);
                        // targetInstructionModel.getInEdges().add(trueEdge);
                        edges.add(trueEdge);

                        ConditionalEdge falseEdge = JBCMM_FACRTORY.createConditionalEdge();
                        falseEdge.setCondition(false);
                        falseEdge.setStart(currentInstructionModel);
                        Instruction fallthroughInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                                jumpInsn.getNext());
                        falseEdge.setEnd(fallthroughInstructionModel);
                        currentInstructionModel.getOutEdges().add(falseEdge);
                        // fallthroughInstructionModel.getInEdges().add(falseEdge);
                        edges.add(falseEdge);
                    }
                } else if (currentInstruction instanceof LookupSwitchInsnNode) {
                    LookupSwitchInsnNode lookupswitchInsn = (LookupSwitchInsnNode) currentInstruction;

                    for (int i = 0; i < lookupswitchInsn.keys.size(); i++) {
                        SwitchCaseEdge switchCaseEdge = JBCMM_FACRTORY.createSwitchCaseEdge();
                        Instruction targetInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                                (LabelNode) lookupswitchInsn.labels.get(i));

                        switchCaseEdge.setCondition((Integer) lookupswitchInsn.keys.get(i));
                        switchCaseEdge.setStart(currentInstructionModel);
                        switchCaseEdge.setEnd(targetInstructionModel);

                        currentInstructionModel.getOutEdges().add(switchCaseEdge);
                        // targetInstructionModel.getInEdges().add(switchCaseEdge);
                        edges.add(switchCaseEdge);
                    }

                    SwitchDefaultEdge switchDefaultEdge = JBCMM_FACRTORY.createSwitchDefaultEdge();
                    Instruction defaultTargetInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                            lookupswitchInsn.dflt);

                    switchDefaultEdge.setStart(currentInstructionModel);
                    switchDefaultEdge.setEnd(defaultTargetInstructionModel);

                    currentInstructionModel.getOutEdges().add(switchDefaultEdge);
                    // defaultTargetInstructionModel.getInEdges().add(switchDefaultEdge);
                    edges.add(switchDefaultEdge);
                } else if (currentInstruction instanceof TableSwitchInsnNode) {
                    TableSwitchInsnNode tableswitchInsn = (TableSwitchInsnNode) currentInstruction;

                    for (int i = tableswitchInsn.min; i <= tableswitchInsn.max; i++) {
                        SwitchCaseEdge switchCaseEdge = JBCMM_FACRTORY.createSwitchCaseEdge();
                        Instruction targetInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                                (LabelNode) tableswitchInsn.labels.get(i - tableswitchInsn.min));

                        switchCaseEdge.setCondition(i);
                        switchCaseEdge.setStart(currentInstructionModel);
                        switchCaseEdge.setEnd(targetInstructionModel);

                        currentInstructionModel.getOutEdges().add(switchCaseEdge);
                        // targetInstructionModel.getInEdges().add(switchCaseEdge);
                        edges.add(switchCaseEdge);
                    }

                    SwitchDefaultEdge switchDefaultEdge = JBCMM_FACRTORY.createSwitchDefaultEdge();
                    Instruction defaultTargetInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                            tableswitchInsn.dflt);

                    switchDefaultEdge.setStart(currentInstructionModel);
                    switchDefaultEdge.setEnd(defaultTargetInstructionModel);

                    currentInstructionModel.getOutEdges().add(switchDefaultEdge);
                    // defaultTargetInstructionModel.getInEdges().add(switchDefaultEdge);
                    edges.add(switchDefaultEdge);
                } else if (!(currentInstruction.getOpcode() == Opcodes.RETURN
                        || currentInstruction.getOpcode() == Opcodes.ARETURN
                        || currentInstruction.getOpcode() == Opcodes.IRETURN
                        || currentInstruction.getOpcode() == Opcodes.LRETURN
                        || currentInstruction.getOpcode() == Opcodes.FRETURN
                        || currentInstruction.getOpcode() == Opcodes.DRETURN
                        || currentInstruction.getOpcode() == Opcodes.ATHROW
                        || currentInstruction.getOpcode() == Opcodes.RET)) {
                    UnconditionalEdge edge = JBCMM_FACRTORY.createUnconditionalEdge();

                    edge.setStart(currentInstructionModel);
                    Instruction nextInstructionModel = findOrCreateNextRealInstruction(insn2Model,
                            currentInstruction.getNext());
                    edge.setEnd(nextInstructionModel);

                    currentInstructionModel.getOutEdges().add(edge);
                    // nextInstructionModel.getInEdges().add(edge);
                    edges.add(edge);
                } else {
                    // return instructions don't have a successor in the control flow graph,
                    // but they may have a successor in the code order
                    try {
                        Instruction nextInstructionInCodeOrderModel = findOrCreateNextRealInstruction(insn2Model,
                                currentInstruction.getNext());
                        ControlFlowEdge nextInCOEdge = JBCMM_FACRTORY.createResumeEdge();
                        nextInCOEdge.setStart(currentInstructionModel);
                        nextInCOEdge.setEnd(nextInstructionInCodeOrderModel);
    
                        currentInstructionModel.getOutEdges().add(nextInCOEdge);
                        // fallthroughInstructionModel.getInEdges().add(ftEdge);
                        edges.add(nextInCOEdge);
                    } catch (IllegalArgumentException e) {
                        // there is no next instruction in code order
                    }
                }

                // specific information
                if (currentInstruction instanceof FieldInsnNode) {
                    FieldInsnNode fieldInsn = (FieldInsnNode) currentInstruction;
                    FieldInstruction fieldInstruction = (FieldInstruction) currentInstructionModel;
                    fieldInstruction
                            .setFieldReference(createFieldReference(fieldInsn.owner, fieldInsn.name, fieldInsn.desc));
                }
                // nothing to be done for instructions without immediate
                // arguments
                // else if (currentInstruction instanceof SimpleInsnNode) {
                // SimpleInsnNode simpleInsn = (SimpleInsnNode)
                // currentInstruction;
                // SimpleInstruction simpleInstruction = (SimpleInstruction)
                // currentInstructionModel;
                // }
                else if (currentInstruction instanceof IntInsnNode) {
                    IntInsnNode intInsn = (IntInsnNode) currentInstruction;
                    IntInstruction intInstruction = (IntInstruction) currentInstructionModel;
                    intInstruction.setOperand(intInsn.operand);
                }
                // invokedynamic not currently supported
                // else if (currentInstruction instanceof InvokeDynamicInsnNode)
                // {
                // throw new IllegalArgumentException("invokedynamic not
                // supported.");
                // InvokeDynamicInsnNode invokedynamicInsn =
                // (InvokeDynamicInsnNode) currentInstruction;
                // InvokeDynamicInstruction invokedynamicInstruction =
                // (InvokeDynamicInstruction) currentInstructionModel;
                // invokedynamicInstruction.setName(invokedynamicInsn.name);
                // invokedynamicInstruction.setDesc(invokedynamicInsn.desc);
                // invokedynamicInstruction.setBsm(invokedynamicInsn.bsm);
                // invokedynamicInstruction.setBsmargs(invokedynamicInsn.bsmargs);
                // }
                else if (currentInstruction instanceof JumpInsnNode) {
                    // nothing to be done, edges are set above
                } else if (currentInstruction instanceof LdcInsnNode) {
                    LdcInsnNode ldcInsn = (LdcInsnNode) currentInstruction;

                    if (currentInstructionModel instanceof LdcIntInstruction) {
                        ((LdcIntInstruction) currentInstructionModel).setConstant((Integer) ldcInsn.cst);
                    } else if (currentInstructionModel instanceof LdcLongInstruction) {
                        ((LdcLongInstruction) currentInstructionModel).setConstant((Long) ldcInsn.cst);
                    } else if (currentInstructionModel instanceof LdcFloatInstruction) {
                        ((LdcFloatInstruction) currentInstructionModel).setConstant((Float) ldcInsn.cst);
                    } else if (currentInstructionModel instanceof LdcDoubleInstruction) {
                        ((LdcDoubleInstruction) currentInstructionModel).setConstant((Double) ldcInsn.cst);
                    } else if (currentInstructionModel instanceof LdcStringInstruction) {
                        String stringConstant = (String) ldcInsn.cst;
                        ((LdcStringInstruction) currentInstructionModel)
                                .setConstant(StringEscapeUtils.escapeJava(stringConstant));
                    } else if (currentInstructionModel instanceof LdcTypeInstruction) {
                        ((LdcTypeInstruction) currentInstructionModel)
                                .setConstant(createTypeReference(((Type) ldcInsn.cst).getDescriptor()));
                    } else {
                        throw new IllegalArgumentException(
                                "Unsupported constant instruction type: " + currentInstructionModel.getClass());
                    }
                } else if (currentInstruction instanceof LookupSwitchInsnNode) {
                    // nothing to be done, edges are set above
                } else if (currentInstruction instanceof MethodInsnNode) {
                    MethodInsnNode methodInsn = (MethodInsnNode) currentInstruction;
                    MethodInstruction methodInstruction = (MethodInstruction) currentInstructionModel;
                    methodInstruction.setMethodReference(
                            createMethodReference(methodInsn.owner, methodInsn.name, methodInsn.desc));
                } else if (currentInstruction instanceof MultiANewArrayInsnNode) {
                    MultiANewArrayInsnNode multianewarrayInsn = (MultiANewArrayInsnNode) currentInstruction;
                    MultianewarrayInstruction multianewarrayInstruction = (MultianewarrayInstruction) currentInstructionModel;
                    multianewarrayInstruction.setTypeReference(
                            createTypeReference(Type.getType(multianewarrayInsn.desc).getDescriptor()));
                } else if (currentInstruction instanceof TableSwitchInsnNode) {
                    // nothing to be done, edges already set above
                } else if (currentInstruction instanceof TypeInsnNode) {
                    TypeInsnNode typeInsn = (TypeInsnNode) currentInstruction;
                    TypeInstruction typeInstruction = (TypeInstruction) currentInstructionModel;
                    typeInstruction
                            .setTypeReference(createTypeReference(Type.getObjectType(typeInsn.desc).getDescriptor()));
                } else if (currentInstruction instanceof VarInsnNode) {
                    VarInsnNode varInsn = (VarInsnNode) currentInstruction;
                    VarInstruction varInstruction = (VarInstruction) currentInstructionModel;
                    LocalVariableTableEntry entry = localVariableModels.get(locals[varInsn.var]);

                    if (varInsn.getOpcode() == Opcodes.ISTORE || varInsn.getOpcode() == Opcodes.LSTORE
                            || varInsn.getOpcode() == Opcodes.FSTORE || varInsn.getOpcode() == Opcodes.DSTORE
                            || varInsn.getOpcode() == Opcodes.ASTORE) {
                        if (varInsn.getNext() instanceof LabelNode) {
                            List<LocalVariableNode> startLabels = startLabel2Local.get((LabelNode) varInsn.getNext());
                            if (startLabels != null) {
                                for (LocalVariableNode localVariableNode : startLabels) {
                                    if (localVariableNode.index == varInsn.var) {
                                        entry = localVariableModels.get(localVariableNode);
                                    }
                                }
                            }
                        }
                    }

                    varInstruction.setLocalVariable(entry);
                    varInstruction.setVarIndex(varInsn.var);
                } else if (currentInstruction instanceof IincInsnNode) {
                    IincInsnNode iincInsn = (IincInsnNode) currentInstruction;
                    IincInstruction iincInstruction = (IincInstruction) currentInstructionModel;
                    LocalVariableTableEntry entry = localVariableModels.get(locals[iincInsn.var]);
                    iincInstruction.setLocalVariable(entry);
                    iincInstruction.setIncr(iincInsn.incr);
                    iincInstruction.setVarIndex(iincInsn.var);
                }
                for (ExceptionTableEntry exceptionTableEntry : exceptionTableEntries) {
                    if (activeExceptionTableEntries.contains(exceptionTableEntry)) {
                        ExceptionalEdge exceptionalEdge = JBCMM_FACRTORY.createExceptionalEdge();
                        currentInstructionModel.getOutEdges().add(exceptionalEdge);
                        exceptionalEdge.setStart(currentInstructionModel);
                        // handlers are not visited yet. They must be resolved after the code
                        exceptionalEdge.setExceptionTableEntry(exceptionTableEntry);
                        exceptionalEdges.add(exceptionalEdge);
                    }
                }
            }

            currentInstruction = currentInstruction.getNext();
        }

        for (ExceptionalEdge exceptionalEdge : exceptionalEdges) {
            Instruction handlerInstructionModel = exceptionalEdge.getExceptionTableEntry().getHandlerInstruction();
            exceptionalEdge.setEnd(handlerInstructionModel);
            // handlerInstructionModel.getInEdges().add(exceptionalEdge);

        }
    }

    private Instruction findOrCreateNextRealInstruction(Map<AbstractInsnNode, Instruction> insn2Model,
            AbstractInsnNode startInsn) {
        AbstractInsnNode current = startInsn;
        while (current != null && ((current instanceof LabelNode) || (current instanceof LineNumberNode)
                || (current instanceof FrameNode))) {
            current = current.getNext();
        }
        if (current == null) {
            throw new IllegalArgumentException("There is no next real instruction.");
        } else {
            return findOrCreateInstruction(insn2Model, current);
        }
    }

    private Instruction findOrCreateInstruction(Map<AbstractInsnNode, Instruction> insn2Model, AbstractInsnNode insn) {
        Instruction result = insn2Model.get(insn);
        if (result == null) {
            if (insn.getOpcode() == Opcodes.GETSTATIC) {
                result = JBCMM_FACRTORY.createGetstaticInstruction();
            } else if (insn.getOpcode() == Opcodes.PUTSTATIC) {
                result = JBCMM_FACRTORY.createPutstaticInstruction();
            } else if (insn.getOpcode() == Opcodes.GETFIELD) {
                result = JBCMM_FACRTORY.createGetfieldInstruction();
            } else if (insn.getOpcode() == Opcodes.PUTFIELD) {
                result = JBCMM_FACRTORY.createPutfieldInstruction();
            } else if (insn.getOpcode() == Opcodes.IINC) {
                result = JBCMM_FACRTORY.createIincInstruction();
            } else if (insn.getOpcode() == Opcodes.NOP) {
                result = JBCMM_FACRTORY.createNopInstruction();
            } else if (insn.getOpcode() == Opcodes.ACONST_NULL) {
                result = JBCMM_FACRTORY.createAconst_nullInstruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_M1) {
                result = JBCMM_FACRTORY.createIconst_m1Instruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_0) {
                result = JBCMM_FACRTORY.createIconst_0Instruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_1) {
                result = JBCMM_FACRTORY.createIconst_1Instruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_2) {
                result = JBCMM_FACRTORY.createIconst_2Instruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_3) {
                result = JBCMM_FACRTORY.createIconst_3Instruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_4) {
                result = JBCMM_FACRTORY.createIconst_4Instruction();
            } else if (insn.getOpcode() == Opcodes.ICONST_5) {
                result = JBCMM_FACRTORY.createIconst_5Instruction();
            } else if (insn.getOpcode() == Opcodes.LCONST_0) {
                result = JBCMM_FACRTORY.createLconst_0Instruction();
            } else if (insn.getOpcode() == Opcodes.LCONST_1) {
                result = JBCMM_FACRTORY.createLconst_1Instruction();
            } else if (insn.getOpcode() == Opcodes.FCONST_0) {
                result = JBCMM_FACRTORY.createFconst_0Instruction();
            } else if (insn.getOpcode() == Opcodes.FCONST_1) {
                result = JBCMM_FACRTORY.createFconst_1Instruction();
            } else if (insn.getOpcode() == Opcodes.FCONST_2) {
                result = JBCMM_FACRTORY.createFconst_2Instruction();
            } else if (insn.getOpcode() == Opcodes.DCONST_0) {
                result = JBCMM_FACRTORY.createDconst_0Instruction();
            } else if (insn.getOpcode() == Opcodes.DCONST_1) {
                result = JBCMM_FACRTORY.createDconst_1Instruction();
            } else if (insn.getOpcode() == Opcodes.IALOAD) {
                result = JBCMM_FACRTORY.createIaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.LALOAD) {
                result = JBCMM_FACRTORY.createLaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.FALOAD) {
                result = JBCMM_FACRTORY.createFaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.DALOAD) {
                result = JBCMM_FACRTORY.createDaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.AALOAD) {
                result = JBCMM_FACRTORY.createAaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.BALOAD) {
                result = JBCMM_FACRTORY.createBaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.CALOAD) {
                result = JBCMM_FACRTORY.createCaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.SALOAD) {
                result = JBCMM_FACRTORY.createSaloadInstruction();
            } else if (insn.getOpcode() == Opcodes.IASTORE) {
                result = JBCMM_FACRTORY.createIastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.LASTORE) {
                result = JBCMM_FACRTORY.createLastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.FASTORE) {
                result = JBCMM_FACRTORY.createFastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.DASTORE) {
                result = JBCMM_FACRTORY.createDastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.AASTORE) {
                result = JBCMM_FACRTORY.createAastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.BASTORE) {
                result = JBCMM_FACRTORY.createBastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.CASTORE) {
                result = JBCMM_FACRTORY.createCastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.SASTORE) {
                result = JBCMM_FACRTORY.createSastoreInstruction();
            } else if (insn.getOpcode() == Opcodes.POP) {
                result = JBCMM_FACRTORY.createPopInstruction();
            } else if (insn.getOpcode() == Opcodes.POP2) {
                result = JBCMM_FACRTORY.createPop2Instruction();
            } else if (insn.getOpcode() == Opcodes.DUP) {
                result = JBCMM_FACRTORY.createDupInstruction();
            } else if (insn.getOpcode() == Opcodes.DUP_X1) {
                result = JBCMM_FACRTORY.createDup_x1Instruction();
            } else if (insn.getOpcode() == Opcodes.DUP_X2) {
                result = JBCMM_FACRTORY.createDup_x2Instruction();
            } else if (insn.getOpcode() == Opcodes.DUP2) {
                result = JBCMM_FACRTORY.createDup2Instruction();
            } else if (insn.getOpcode() == Opcodes.DUP2_X1) {
                result = JBCMM_FACRTORY.createDup2_x1Instruction();
            } else if (insn.getOpcode() == Opcodes.DUP2_X2) {
                result = JBCMM_FACRTORY.createDup2_x2Instruction();
            } else if (insn.getOpcode() == Opcodes.SWAP) {
                result = JBCMM_FACRTORY.createSwapInstruction();
            } else if (insn.getOpcode() == Opcodes.IADD) {
                result = JBCMM_FACRTORY.createIaddInstruction();
            } else if (insn.getOpcode() == Opcodes.LADD) {
                result = JBCMM_FACRTORY.createLaddInstruction();
            } else if (insn.getOpcode() == Opcodes.FADD) {
                result = JBCMM_FACRTORY.createFaddInstruction();
            } else if (insn.getOpcode() == Opcodes.DADD) {
                result = JBCMM_FACRTORY.createDaddInstruction();
            } else if (insn.getOpcode() == Opcodes.ISUB) {
                result = JBCMM_FACRTORY.createIsubInstruction();
            } else if (insn.getOpcode() == Opcodes.LSUB) {
                result = JBCMM_FACRTORY.createLsubInstruction();
            } else if (insn.getOpcode() == Opcodes.FSUB) {
                result = JBCMM_FACRTORY.createFsubInstruction();
            } else if (insn.getOpcode() == Opcodes.DSUB) {
                result = JBCMM_FACRTORY.createDsubInstruction();
            } else if (insn.getOpcode() == Opcodes.IMUL) {
                result = JBCMM_FACRTORY.createImulInstruction();
            } else if (insn.getOpcode() == Opcodes.LMUL) {
                result = JBCMM_FACRTORY.createLmulInstruction();
            } else if (insn.getOpcode() == Opcodes.FMUL) {
                result = JBCMM_FACRTORY.createFmulInstruction();
            } else if (insn.getOpcode() == Opcodes.DMUL) {
                result = JBCMM_FACRTORY.createDmulInstruction();
            } else if (insn.getOpcode() == Opcodes.IDIV) {
                result = JBCMM_FACRTORY.createIdivInstruction();
            } else if (insn.getOpcode() == Opcodes.LDIV) {
                result = JBCMM_FACRTORY.createLdivInstruction();
            } else if (insn.getOpcode() == Opcodes.FDIV) {
                result = JBCMM_FACRTORY.createFdivInstruction();
            } else if (insn.getOpcode() == Opcodes.DDIV) {
                result = JBCMM_FACRTORY.createDdivInstruction();
            } else if (insn.getOpcode() == Opcodes.IREM) {
                result = JBCMM_FACRTORY.createIremInstruction();
            } else if (insn.getOpcode() == Opcodes.LREM) {
                result = JBCMM_FACRTORY.createLremInstruction();
            } else if (insn.getOpcode() == Opcodes.FREM) {
                result = JBCMM_FACRTORY.createFremInstruction();
            } else if (insn.getOpcode() == Opcodes.DREM) {
                result = JBCMM_FACRTORY.createDremInstruction();
            } else if (insn.getOpcode() == Opcodes.INEG) {
                result = JBCMM_FACRTORY.createInegInstruction();
            } else if (insn.getOpcode() == Opcodes.LNEG) {
                result = JBCMM_FACRTORY.createLnegInstruction();
            } else if (insn.getOpcode() == Opcodes.FNEG) {
                result = JBCMM_FACRTORY.createFnegInstruction();
            } else if (insn.getOpcode() == Opcodes.DNEG) {
                result = JBCMM_FACRTORY.createDnegInstruction();
            } else if (insn.getOpcode() == Opcodes.ISHL) {
                result = JBCMM_FACRTORY.createIshlInstruction();
            } else if (insn.getOpcode() == Opcodes.LSHL) {
                result = JBCMM_FACRTORY.createLshlInstruction();
            } else if (insn.getOpcode() == Opcodes.ISHR) {
                result = JBCMM_FACRTORY.createIshrInstruction();
            } else if (insn.getOpcode() == Opcodes.LSHR) {
                result = JBCMM_FACRTORY.createLshrInstruction();
            } else if (insn.getOpcode() == Opcodes.IUSHR) {
                result = JBCMM_FACRTORY.createIushrInstruction();
            } else if (insn.getOpcode() == Opcodes.LUSHR) {
                result = JBCMM_FACRTORY.createLushrInstruction();
            } else if (insn.getOpcode() == Opcodes.IAND) {
                result = JBCMM_FACRTORY.createIandInstruction();
            } else if (insn.getOpcode() == Opcodes.LAND) {
                result = JBCMM_FACRTORY.createLandInstruction();
            } else if (insn.getOpcode() == Opcodes.IOR) {
                result = JBCMM_FACRTORY.createIorInstruction();
            } else if (insn.getOpcode() == Opcodes.LOR) {
                result = JBCMM_FACRTORY.createLorInstruction();
            } else if (insn.getOpcode() == Opcodes.IXOR) {
                result = JBCMM_FACRTORY.createIxorInstruction();
            } else if (insn.getOpcode() == Opcodes.LXOR) {
                result = JBCMM_FACRTORY.createLxorInstruction();
            } else if (insn.getOpcode() == Opcodes.I2L) {
                result = JBCMM_FACRTORY.createI2lInstruction();
            } else if (insn.getOpcode() == Opcodes.I2F) {
                result = JBCMM_FACRTORY.createI2fInstruction();
            } else if (insn.getOpcode() == Opcodes.I2D) {
                result = JBCMM_FACRTORY.createI2dInstruction();
            } else if (insn.getOpcode() == Opcodes.L2I) {
                result = JBCMM_FACRTORY.createL2iInstruction();
            } else if (insn.getOpcode() == Opcodes.L2F) {
                result = JBCMM_FACRTORY.createL2fInstruction();
            } else if (insn.getOpcode() == Opcodes.L2D) {
                result = JBCMM_FACRTORY.createL2dInstruction();
            } else if (insn.getOpcode() == Opcodes.F2I) {
                result = JBCMM_FACRTORY.createF2iInstruction();
            } else if (insn.getOpcode() == Opcodes.F2L) {
                result = JBCMM_FACRTORY.createF2lInstruction();
            } else if (insn.getOpcode() == Opcodes.F2D) {
                result = JBCMM_FACRTORY.createF2dInstruction();
            } else if (insn.getOpcode() == Opcodes.D2I) {
                result = JBCMM_FACRTORY.createD2iInstruction();
            } else if (insn.getOpcode() == Opcodes.D2L) {
                result = JBCMM_FACRTORY.createD2lInstruction();
            } else if (insn.getOpcode() == Opcodes.D2F) {
                result = JBCMM_FACRTORY.createD2fInstruction();
            } else if (insn.getOpcode() == Opcodes.I2B) {
                result = JBCMM_FACRTORY.createI2bInstruction();
            } else if (insn.getOpcode() == Opcodes.I2C) {
                result = JBCMM_FACRTORY.createI2cInstruction();
            } else if (insn.getOpcode() == Opcodes.I2S) {
                result = JBCMM_FACRTORY.createI2sInstruction();
            } else if (insn.getOpcode() == Opcodes.LCMP) {
                result = JBCMM_FACRTORY.createLcmpInstruction();
            } else if (insn.getOpcode() == Opcodes.FCMPL) {
                result = JBCMM_FACRTORY.createFcmplInstruction();
            } else if (insn.getOpcode() == Opcodes.FCMPG) {
                result = JBCMM_FACRTORY.createFcmpgInstruction();
            } else if (insn.getOpcode() == Opcodes.DCMPL) {
                result = JBCMM_FACRTORY.createDcmplInstruction();
            } else if (insn.getOpcode() == Opcodes.DCMPG) {
                result = JBCMM_FACRTORY.createDcmpgInstruction();
            } else if (insn.getOpcode() == Opcodes.IRETURN) {
                result = JBCMM_FACRTORY.createIreturnInstruction();
            } else if (insn.getOpcode() == Opcodes.LRETURN) {
                result = JBCMM_FACRTORY.createLreturnInstruction();
            } else if (insn.getOpcode() == Opcodes.FRETURN) {
                result = JBCMM_FACRTORY.createFreturnInstruction();
            } else if (insn.getOpcode() == Opcodes.DRETURN) {
                result = JBCMM_FACRTORY.createDreturnInstruction();
            } else if (insn.getOpcode() == Opcodes.ARETURN) {
                result = JBCMM_FACRTORY.createAreturnInstruction();
            } else if (insn.getOpcode() == Opcodes.RETURN) {
                result = JBCMM_FACRTORY.createReturnInstruction();
            } else if (insn.getOpcode() == Opcodes.ARRAYLENGTH) {
                result = JBCMM_FACRTORY.createArraylengthInstruction();
            } else if (insn.getOpcode() == Opcodes.ATHROW) {
                result = JBCMM_FACRTORY.createAthrowInstruction();
            } else if (insn.getOpcode() == Opcodes.MONITORENTER) {
                result = JBCMM_FACRTORY.createMonitorenterInstruction();
            } else if (insn.getOpcode() == Opcodes.MONITOREXIT) {
                result = JBCMM_FACRTORY.createMonitorexitInstruction();
            } else if (insn.getOpcode() == Opcodes.BIPUSH) {
                result = JBCMM_FACRTORY.createBipushInstruction();
            } else if (insn.getOpcode() == Opcodes.SIPUSH) {
                result = JBCMM_FACRTORY.createSipushInstruction();
            } else if (insn.getOpcode() == Opcodes.NEWARRAY) {
                result = JBCMM_FACRTORY.createNewarrayInstruction();
            } else if (insn.getOpcode() == Opcodes.INVOKEDYNAMIC) {
                throw new IllegalArgumentException("invokedynamic currently not supported");
            } else if (insn.getOpcode() == Opcodes.IFEQ) {
                result = JBCMM_FACRTORY.createIfeqInstruction();
            } else if (insn.getOpcode() == Opcodes.IFNE) {
                result = JBCMM_FACRTORY.createIfneInstruction();
            } else if (insn.getOpcode() == Opcodes.IFLT) {
                result = JBCMM_FACRTORY.createIfltInstruction();
            } else if (insn.getOpcode() == Opcodes.IFGE) {
                result = JBCMM_FACRTORY.createIfgeInstruction();
            } else if (insn.getOpcode() == Opcodes.IFGT) {
                result = JBCMM_FACRTORY.createIfgtInstruction();
            } else if (insn.getOpcode() == Opcodes.IFLE) {
                result = JBCMM_FACRTORY.createIfleInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ICMPEQ) {
                result = JBCMM_FACRTORY.createIf_icmpeqInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ICMPNE) {
                result = JBCMM_FACRTORY.createIf_icmpneInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ICMPLT) {
                result = JBCMM_FACRTORY.createIf_icmpltInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ICMPGE) {
                result = JBCMM_FACRTORY.createIf_icmpgeInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ICMPGT) {
                result = JBCMM_FACRTORY.createIf_icmpgtInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ICMPLE) {
                result = JBCMM_FACRTORY.createIf_icmpleInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ACMPEQ) {
                result = JBCMM_FACRTORY.createIf_acmpeqInstruction();
            } else if (insn.getOpcode() == Opcodes.IF_ACMPNE) {
                result = JBCMM_FACRTORY.createIf_acmpneInstruction();
            } else if (insn.getOpcode() == Opcodes.GOTO) {
                result = JBCMM_FACRTORY.createGotoInstruction();
            } else if (insn.getOpcode() == Opcodes.JSR) {
                result = JBCMM_FACRTORY.createJsrInstruction();
            } else if (insn.getOpcode() == Opcodes.IFNULL) {
                result = JBCMM_FACRTORY.createIfnullInstruction();
            } else if (insn.getOpcode() == Opcodes.IFNONNULL) {
                result = JBCMM_FACRTORY.createIfnonnullInstruction();
            } else if (insn.getOpcode() == Opcodes.LDC) {
                LdcInsnNode ldcInsn = (LdcInsnNode) insn;
                if (ldcInsn.cst instanceof Integer)
                    result = JBCMM_FACRTORY.createLdcIntInstruction();
                else if (ldcInsn.cst instanceof Long)
                    result = JBCMM_FACRTORY.createLdcLongInstruction();
                else if (ldcInsn.cst instanceof Float)
                    result = JBCMM_FACRTORY.createLdcFloatInstruction();
                else if (ldcInsn.cst instanceof Double)
                    result = JBCMM_FACRTORY.createLdcDoubleInstruction();
                else if (ldcInsn.cst instanceof String)
                    result = JBCMM_FACRTORY.createLdcStringInstruction();
                else if (ldcInsn.cst instanceof Type)
                    result = JBCMM_FACRTORY.createLdcTypeInstruction();
                else
                    throw new IllegalArgumentException("Unsupported type of constant value: " + ldcInsn.cst.getClass());
            } else if (insn.getOpcode() == Opcodes.LOOKUPSWITCH) {
                result = JBCMM_FACRTORY.createSwitchInstruction();
            } else if (insn.getOpcode() == Opcodes.INVOKEVIRTUAL) {
                result = JBCMM_FACRTORY.createInvokevirtualInstruction();
            } else if (insn.getOpcode() == Opcodes.INVOKESPECIAL) {
                result = JBCMM_FACRTORY.createInvokespecialInstruction();
            } else if (insn.getOpcode() == Opcodes.INVOKESTATIC) {
                result = JBCMM_FACRTORY.createInvokestaticInstruction();
            } else if (insn.getOpcode() == Opcodes.INVOKEINTERFACE) {
                result = JBCMM_FACRTORY.createInvokeinterfaceInstruction();
            } else if (insn.getOpcode() == Opcodes.MULTIANEWARRAY) {
                result = JBCMM_FACRTORY.createMultianewarrayInstruction();
            } else if (insn.getOpcode() == Opcodes.TABLESWITCH) {
                result = JBCMM_FACRTORY.createSwitchInstruction();
            } else if (insn.getOpcode() == Opcodes.NEW) {
                result = JBCMM_FACRTORY.createNewInstruction();
            } else if (insn.getOpcode() == Opcodes.ANEWARRAY) {
                result = JBCMM_FACRTORY.createAnewarrayInstruction();
            } else if (insn.getOpcode() == Opcodes.CHECKCAST) {
                result = JBCMM_FACRTORY.createCheckcastInstruction();
            } else if (insn.getOpcode() == Opcodes.INSTANCEOF) {
                result = JBCMM_FACRTORY.createInstanceofInstruction();
            } else if (insn.getOpcode() == Opcodes.ILOAD) {
                result = JBCMM_FACRTORY.createIloadInstruction();
            } else if (insn.getOpcode() == Opcodes.LLOAD) {
                result = JBCMM_FACRTORY.createLloadInstruction();
            } else if (insn.getOpcode() == Opcodes.FLOAD) {
                result = JBCMM_FACRTORY.createFloadInstruction();
            } else if (insn.getOpcode() == Opcodes.DLOAD) {
                result = JBCMM_FACRTORY.createDloadInstruction();
            } else if (insn.getOpcode() == Opcodes.ALOAD) {
                result = JBCMM_FACRTORY.createAloadInstruction();
            } else if (insn.getOpcode() == Opcodes.ISTORE) {
                result = JBCMM_FACRTORY.createIstoreInstruction();
            } else if (insn.getOpcode() == Opcodes.LSTORE) {
                result = JBCMM_FACRTORY.createLstoreInstruction();
            } else if (insn.getOpcode() == Opcodes.FSTORE) {
                result = JBCMM_FACRTORY.createFstoreInstruction();
            } else if (insn.getOpcode() == Opcodes.DSTORE) {
                result = JBCMM_FACRTORY.createDstoreInstruction();
            } else if (insn.getOpcode() == Opcodes.ASTORE) {
                result = JBCMM_FACRTORY.createAstoreInstruction();
            } else if (insn.getOpcode() == Opcodes.RET) {
                result = JBCMM_FACRTORY.createRetInstruction();
            } else {
                throw new IllegalArgumentException("Uncupported opcode: " + insn.getOpcode());
            }

            insn2Model.put(insn, result);
        }
        return result;
    }

}
