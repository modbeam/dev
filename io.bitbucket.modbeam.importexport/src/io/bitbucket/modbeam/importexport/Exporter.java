package io.bitbucket.modbeam.importexport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.CheckClassAdapter;

import io.bitbucket.modbeam.jbc.Annotation;
import io.bitbucket.modbeam.jbc.AreturnInstruction;
import io.bitbucket.modbeam.jbc.ArrayTypeSignature;
import io.bitbucket.modbeam.jbc.AthrowInstruction;
import io.bitbucket.modbeam.jbc.Basetype;
import io.bitbucket.modbeam.jbc.BooleanValue;
import io.bitbucket.modbeam.jbc.ByteValue;
import io.bitbucket.modbeam.jbc.CharValue;
import io.bitbucket.modbeam.jbc.ClassSignature;
import io.bitbucket.modbeam.jbc.ClassTypeSignature;
import io.bitbucket.modbeam.jbc.Clazz;
import io.bitbucket.modbeam.jbc.ConditionalEdge;
import io.bitbucket.modbeam.jbc.ControlFlowEdge;
import io.bitbucket.modbeam.jbc.DoubleValue;
import io.bitbucket.modbeam.jbc.DreturnInstruction;
import io.bitbucket.modbeam.jbc.ElementValue;
import io.bitbucket.modbeam.jbc.ElementValuePair;
import io.bitbucket.modbeam.jbc.ElementaryValue;
import io.bitbucket.modbeam.jbc.ExceptionTableEntry;
import io.bitbucket.modbeam.jbc.Field;
import io.bitbucket.modbeam.jbc.FieldInstruction;
import io.bitbucket.modbeam.jbc.FieldReference;
import io.bitbucket.modbeam.jbc.FieldTypeSignature;
import io.bitbucket.modbeam.jbc.FloatValue;
import io.bitbucket.modbeam.jbc.FormalTypeParameter;
import io.bitbucket.modbeam.jbc.FreturnInstruction;
import io.bitbucket.modbeam.jbc.GotoInstruction;
import io.bitbucket.modbeam.jbc.IincInstruction;
import io.bitbucket.modbeam.jbc.Instruction;
import io.bitbucket.modbeam.jbc.IntInstruction;
import io.bitbucket.modbeam.jbc.IntValue;
import io.bitbucket.modbeam.jbc.IreturnInstruction;
import io.bitbucket.modbeam.jbc.JbcPackage;
import io.bitbucket.modbeam.jbc.JsrInstruction;
import io.bitbucket.modbeam.jbc.JumpInstruction;
import io.bitbucket.modbeam.jbc.LdcDoubleInstruction;
import io.bitbucket.modbeam.jbc.LdcFloatInstruction;
import io.bitbucket.modbeam.jbc.LdcIntInstruction;
import io.bitbucket.modbeam.jbc.LdcLongInstruction;
import io.bitbucket.modbeam.jbc.LdcStringInstruction;
import io.bitbucket.modbeam.jbc.LdcTypeInstruction;
import io.bitbucket.modbeam.jbc.LocalVariableTableEntry;
import io.bitbucket.modbeam.jbc.LongValue;
import io.bitbucket.modbeam.jbc.LreturnInstruction;
import io.bitbucket.modbeam.jbc.Method;
import io.bitbucket.modbeam.jbc.MethodInstruction;
import io.bitbucket.modbeam.jbc.MethodReference;
import io.bitbucket.modbeam.jbc.MethodSignature;
import io.bitbucket.modbeam.jbc.MultianewarrayInstruction;
import io.bitbucket.modbeam.jbc.ResumeEdge;
import io.bitbucket.modbeam.jbc.RetInstruction;
import io.bitbucket.modbeam.jbc.ReturnInstruction;
import io.bitbucket.modbeam.jbc.ReturnType;
import io.bitbucket.modbeam.jbc.ShortValue;
import io.bitbucket.modbeam.jbc.SimpleClassTypeSignature;
import io.bitbucket.modbeam.jbc.StringValue;
import io.bitbucket.modbeam.jbc.SwitchCaseEdge;
import io.bitbucket.modbeam.jbc.SwitchDefaultEdge;
import io.bitbucket.modbeam.jbc.SwitchInstruction;
import io.bitbucket.modbeam.jbc.ThrowsSignature;
import io.bitbucket.modbeam.jbc.TypeArgument;
import io.bitbucket.modbeam.jbc.TypeConcrete;
import io.bitbucket.modbeam.jbc.TypeInstruction;
import io.bitbucket.modbeam.jbc.TypeSignature;
import io.bitbucket.modbeam.jbc.TypeVariableSignature;
import io.bitbucket.modbeam.jbc.TypeWild;
import io.bitbucket.modbeam.jbc.UnconditionalEdge;
import io.bitbucket.modbeam.jbc.VarInstruction;
import io.bitbucket.modbeam.jbc.VoidDescriptor;

public class Exporter {

    public String export(Clazz clazz, String generatedRootDir) {
        File baseFolder = new File(generatedRootDir);
        baseFolder.mkdirs();
        String className = clazz.getName();
        File classFile;
        String relativeResultFileName;
        if (className.contains("/")) {
            String packageName = className.substring(0, className.lastIndexOf("/"));
            String packageDirName = packageName.replace('/', File.separatorChar);
            File packageDir = new File(baseFolder, packageDirName);
            packageDir.mkdirs();
            String classFileName = className.substring(className.lastIndexOf("/") + 1) + ".class";
            classFile = new File(packageDir, classFileName);
            relativeResultFileName = packageDirName + File.separator + classFileName;
        } else {
            classFile = new File(baseFolder, className + ".class");
            relativeResultFileName = className + ".class";
        }
        try {
            classFile.createNewFile();

            export(clazz, classFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return relativeResultFileName;
    }

    private void export(Clazz clazz, File classFile) throws IOException {
        int flags = ClassWriter.COMPUTE_MAXS;
        if (clazz.getMajorVersion() >= 51) // Java 6
            flags = ClassWriter.COMPUTE_FRAMES;

        ClassWriter classWriter = new ClassWriter(flags);
        CheckClassAdapter checkedClassWriter = new CheckClassAdapter(classWriter, false);

        assemble(checkedClassWriter, clazz);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        byte[] bytecode = classWriter.toByteArray();

        try {
            CheckClassAdapter.verify(new ClassReader(bytecode), true, pw);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(sw.toString());
        }

        try (FileOutputStream fos = new FileOutputStream(classFile)) {
            fos.write(bytecode);
        }
    }

    private void assemble(ClassVisitor classVisitor, Clazz clazz) {
        int access = 0;
        if (clazz.isSuper())
            access |= Opcodes.ACC_SUPER;
        if (clazz.isAbstract())
            access |= Opcodes.ACC_ABSTRACT;
        if (clazz.isAnnotation())
            access |= Opcodes.ACC_ANNOTATION;
        if (clazz.isDeprecated())
            access |= Opcodes.ACC_DEPRECATED;
        if (clazz.isEnum())
            access |= Opcodes.ACC_ENUM;
        if (clazz.isFinal())
            access |= Opcodes.ACC_FINAL;
        if (clazz.isInterface())
            access |= Opcodes.ACC_INTERFACE;
        if (clazz.isPrivate())
            access |= Opcodes.ACC_PRIVATE;
        if (clazz.isProtected())
            access |= Opcodes.ACC_PROTECTED;
        if (clazz.isPublic())
            access |= Opcodes.ACC_PUBLIC;
        if (clazz.isSynthetic())
            access |= Opcodes.ACC_SYNTHETIC;

        String[] interfaces = new String[clazz.getInterfaces().size()];
        for (int i = 0; i < clazz.getInterfaces().size(); i++) {
            interfaces[i] = Type.getType(clazz.getInterfaces().get(i).getTypeDescriptor()).getInternalName();
        }

        /*
         * Aufruf einer neuer ClassSignature
         */
        String classSignature = null;
        if (clazz.getSignature() != null) {
            classSignature = buildClassSignatureString(clazz.getSignature());
        }

        String superName = null;
        if (clazz.getSuperClass() != null)
            superName = Type.getType(clazz.getSuperClass().getTypeDescriptor()).getInternalName();

        classVisitor.visit(clazz.getMinorVersion() << 16 | clazz.getMajorVersion(), access, clazz.getName(),
                classSignature, superName, interfaces);

        classVisitor.visitSource(clazz.getSourceFileName(), clazz.getSourceDebugExtension());
        if (clazz.getEnclosingMethod() != null) {
            Type resultType = Type
                    .getType(clazz.getEnclosingMethod().getDescriptor().getResultType().getTypeDescriptor());
            Type[] argumentTypes = new Type[clazz.getEnclosingMethod().getDescriptor().getParameterTypes().size()];
            for (int i = 0; i < clazz.getEnclosingMethod().getDescriptor().getParameterTypes().size(); i++) {
                argumentTypes[i] = Type.getType(
                        clazz.getEnclosingMethod().getDescriptor().getParameterTypes().get(i).getTypeDescriptor());
            }

            classVisitor.visitOuterClass(
                    Type.getType(clazz.getEnclosingMethod().getDeclaringClass().getTypeDescriptor()).getInternalName(),
                    clazz.getEnclosingMethod().getName(), Type.getMethodDescriptor(resultType, argumentTypes));
        } else if (clazz.getOuterClass() != null) {
            classVisitor.visitOuterClass(Type.getType(clazz.getOuterClass().getTypeDescriptor()).getInternalName(),
                    null, null);
        }

        if (clazz.getRuntimeInvisibleAnnotations() != null) {
            for (Annotation annot : clazz.getRuntimeInvisibleAnnotations()) {
                AnnotationVisitor annotVisitor = classVisitor.visitAnnotation(annot.getType().getTypeDescriptor(), false);
                assembleAnnotation(annotVisitor, annot, clazz.getName());
            }
        }
        if (clazz.getRuntimeVisibleAnnotations() != null) {
            for (Annotation annot : clazz.getRuntimeVisibleAnnotations()) {
                AnnotationVisitor annotVisitor = classVisitor.visitAnnotation(annot.getType().getTypeDescriptor(), true);
                assembleAnnotation(annotVisitor, annot, clazz.getName());
            }
        }

        // classNode.visitTypeAnnotation(typeRef, typePath, desc, visible);
        // classNode.visitAttribute(attr);
        // classNode.visitInnerClass(name, outerName, innerName, access);

        assembleFields(classVisitor, clazz);
        assembleMethods(classVisitor, clazz);
        classVisitor.visitEnd();
    }

    //
    //
    //
    // Signature Private Methods
    //
    //
    //

    // ----- ClassSignature -----<
    private String buildClassSignatureString(ClassSignature cs) {
        String classSignatureString = "";
        List<String> formalTypeParameters = new ArrayList<>();
        String superClassSignatureString = buildClassTypeSignatureString(cs.getSuperClassSignature());
        List<String> superInterfaceSignatures = new ArrayList<>();

        for (FormalTypeParameter ftp : cs.getFormalTypeParameter()) {
            formalTypeParameters.add(buildFormalTypeParameterString(ftp));
        }

        for (ClassTypeSignature cts : cs.getSuperInterfaceSignature()) {
            superInterfaceSignatures.add(buildClassTypeSignatureString(cts));
        }

        if (!formalTypeParameters.isEmpty()) {
            classSignatureString = "<";
            for (String ftp : formalTypeParameters) {
                classSignatureString += ftp;
            }
            classSignatureString += ">";
        }
        classSignatureString += superClassSignatureString;

        if (!superInterfaceSignatures.isEmpty()) {

            for (String sts : superInterfaceSignatures) {
                classSignatureString += sts;
            }
        }
        // classSignatureString += ";";
        return classSignatureString;
    }

    // ----- MethodSignature -----<
    private String buildMethodSignature(MethodSignature ms) {
        String methodSignatureString = "";
        List<String> formalTypeParameters = new ArrayList<>();
        List<String> parameterTypes = new ArrayList<>();
        String returnTypeString = buildReturnTypeString(ms.getReturnType());
        //
        List<String> throwsSignatures = new ArrayList<>();
        //

        for (FormalTypeParameter ftp : ms.getFormalTypeParameter()) {
            formalTypeParameters.add(buildFormalTypeParameterString(ftp));
        }
        for (TypeSignature ts : ms.getTypeSignature()) {
            parameterTypes.add(buildTypeSignatureString(ts));
        }
        //
        for (ThrowsSignature ts: ms.getThrowsSignature()) {
        	throwsSignatures.add(buildThrowsSignatureString(ts));
        }
        //
        	
        if (!formalTypeParameters.isEmpty()) {
            methodSignatureString = "<";
            for (String ftp : formalTypeParameters) {
                methodSignatureString += ftp;
            }
            methodSignatureString += ">";
        }

        methodSignatureString += "(";
        if (!parameterTypes.isEmpty()) {
            for (String ts : parameterTypes) {
                methodSignatureString += ts;
            }
        }
        methodSignatureString += ")" + returnTypeString;
        
        //
        if(!throwsSignatures.isEmpty()) {
        	for (String ts: throwsSignatures) {
        		methodSignatureString += ts;
        	}
        }
        //
        return methodSignatureString;
    }

    // ----- FormalParameterSignature -----<
    private String buildFormalTypeParameterString(FormalTypeParameter ftp) {
        String formalTypeParameterString = "";
        String identifierString = ftp.getIdentifier();
        String classBoundString = buildFieldTypeString(ftp.getClassBound());
        List<String> interfaceBounds = new ArrayList<>();

        for (FieldTypeSignature fts : ftp.getInterfaceBound()) {
            interfaceBounds.add(buildFieldTypeString(fts));
        }

        formalTypeParameterString = identifierString + ":" + classBoundString;
        if (!interfaceBounds.isEmpty()) {
        	for (String ib : interfaceBounds) {
                formalTypeParameterString += ":" + ib;
            }
        }
        return formalTypeParameterString;
    }

    // ----- ReturnType -----<
    private String buildReturnTypeString(ReturnType rt) {
        String returnTypeString = "";

        if (rt instanceof TypeSignature) {
            returnTypeString = buildTypeSignatureString((TypeSignature) rt);
        } else {
            returnTypeString = buildVoidDescriptorString((VoidDescriptor) rt);
        }
        return returnTypeString;
    }

    // ----- TypeSignature -----<
    private String buildTypeSignatureString(TypeSignature ts) {
        String typeSignatureString = "";

        if (ts instanceof FieldTypeSignature) {
            typeSignatureString = buildFieldTypeString((FieldTypeSignature) ts);
        } else {
            typeSignatureString = buildBaseTypeString((Basetype) ts);
        }
        return typeSignatureString;
    }

    // ----- FieldTypeSignature -----<
    private String buildFieldTypeString(FieldTypeSignature fts) {
        String fieldTypeSignatureString = "";

        if (fts instanceof ClassTypeSignature) {
            fieldTypeSignatureString = buildClassTypeSignatureString((ClassTypeSignature) fts);
        } else if (fts instanceof TypeVariableSignature) {
            fieldTypeSignatureString = buildTypeVariableSignatureString((TypeVariableSignature) fts);
        } else {
            fieldTypeSignatureString = buildArrayTypeSignatureString((ArrayTypeSignature) fts);
        }
        return fieldTypeSignatureString;
    }

    // ----- ClassTypeSignature -----<
    private String buildClassTypeSignatureString(ClassTypeSignature cts) {
        Boolean hasPackage = false;
        String classTypeSignatureString = "";
        String packageSpecifierString = "";
        List<String> simpleClassSignatureSuffixes = new ArrayList<>();

        if (cts.getPackagespecifier() != null) {
            packageSpecifierString += cts.getPackagespecifier();
            hasPackage = true;
        }

        String simpleClassSignatureString = buildSimpleClassTypeSignatureString(cts.getSimpleClassSignature(),
                hasPackage);
        for (SimpleClassTypeSignature scts : cts.getSimpleClassSignatureSuffix()) {
            simpleClassSignatureSuffixes.add(buildSimpleClassTypeSignatureString(scts, false));
        }

        classTypeSignatureString = "L" + packageSpecifierString + simpleClassSignatureString;
        if (!simpleClassSignatureSuffixes.isEmpty()) {

            for (String scts : simpleClassSignatureSuffixes) {
                classTypeSignatureString += "." + scts;
            }
        }
        classTypeSignatureString += ";";
        return classTypeSignatureString;
    }

    // ----- SimpleClassTypeSignature -----<
    private String buildSimpleClassTypeSignatureString(SimpleClassTypeSignature scts, Boolean hasPackage) {
        String simpleClassTypeSignatureString = "";
        String identifier = scts.getIdentifier();
        List<String> typeArguments = new ArrayList<>();

        for (TypeArgument ta : scts.getTypeArgument()) {
            typeArguments.add(buildTypeArgumentString(ta));
        }

        if (hasPackage) {
            simpleClassTypeSignatureString = "/" + identifier;
        } else {
            simpleClassTypeSignatureString = identifier;
        }
        if (!typeArguments.isEmpty()) {
            simpleClassTypeSignatureString += "<";
            for (String ta : typeArguments) {
                simpleClassTypeSignatureString += ta;
            }
            simpleClassTypeSignatureString += ">";
        }
        return simpleClassTypeSignatureString;
    }

    // ----- ThrowsSignature -----<
    private String buildThrowsSignatureString(ThrowsSignature ts) {
    	String result = "";
    	
    	if(ts instanceof TypeVariableSignature tvs) {
    		result += "^"+buildTypeVariableSignatureString(tvs);
    	} else if(ts instanceof ClassTypeSignature cts) {
    		result += "^"+buildClassTypeSignatureString(cts);
    	}
    	return result;
    }
    
    // ----- TypeArgument -----<
    private String buildTypeArgumentString(TypeArgument ta) {
        String typeArgumentString = "";

        if (ta instanceof TypeConcrete) {
            typeArgumentString = buildTypeConcreteString((TypeConcrete) ta);
        } else {
            typeArgumentString = buildTypeWildString((TypeWild) ta);
        }
        return typeArgumentString;
    }

    // ----- TypeConcrete -----<
    private String buildTypeConcreteString(TypeConcrete tc) {
        String typeConcreteString = "";
        String indicatorString = "";
        String fieldTypeSignatureString = buildFieldTypeString(tc.getFieldTypeSignature());

        if (tc.getWildcardIndicator().name() == "SUB") {
            indicatorString = "+";
        } else if (tc.getWildcardIndicator().name() == "SUPER") {
            indicatorString = "-";
        } else {
            indicatorString = "";
        }

        typeConcreteString = indicatorString + fieldTypeSignatureString;
        return typeConcreteString;
    }

    // ----- TypeWild -----<
    private String buildTypeWildString(TypeWild tw) {
        String typeWildString = "*";
        return typeWildString;
    }

    // ----- TypeVariableSignature -----<
    private String buildTypeVariableSignatureString(TypeVariableSignature tvs) {
        String typeVarbiableSignatureString = "T" + tvs.getIdentifier() + ";";
        return typeVarbiableSignatureString;
    }

    // ----- ArrayTypeSignature -----<
    private String buildArrayTypeSignatureString(ArrayTypeSignature ats) {
        String arrayTypeSignatureString = "";
        String dim = "[";
        String dimension = new String(new char[ats.getDimension()]).replace("\0", dim);
        String typeSignatureString = buildTypeSignatureString(ats.getTypeSignature());
        arrayTypeSignatureString = dimension + typeSignatureString;
        return arrayTypeSignatureString;
    }

    // ----- BaseType -----<
    private String buildBaseTypeString(Basetype bt) {
        String baseTypeString = bt.getType().name();
        String returnString = "";

        switch (baseTypeString) {
        case "INTEGER":
            returnString = "I";
            break;
        case "BYTE":
            returnString = "B";
            break;
        case "CHAR":
            returnString = "C";
            break;
        case "BOOLEAN":
            returnString = "Z";
            break;
        case "SHORT":
            returnString = "S";
            break;
        case "FLOAT":
            returnString = "F";
            break;
        case "DOUBLE":
            returnString = "D";
            break;
        case "LONG":
            returnString = "J";
            break;
        }
        return returnString;
    }

    // ----- VoidDescriptor -----<
    private String buildVoidDescriptorString(VoidDescriptor vd) {
        String voidDescriptorString = "V";
        return voidDescriptorString;
    }

    //
    //
    //
    // Signature Private Methods (ending)
    //
    //
    //

    private void assembleMethods(ClassVisitor classVisitor, Clazz clazz) {
        for (Method method : clazz.getMethods()) {
            int methodAccess = 0;
            if (method.isAbstract())
                methodAccess |= Opcodes.ACC_ABSTRACT;
            if (method.isDeprecated())
                methodAccess |= Opcodes.ACC_DEPRECATED;
            if (method.isBridge())
                methodAccess |= Opcodes.ACC_BRIDGE;
            if (method.isFinal())
                methodAccess |= Opcodes.ACC_FINAL;
            if (method.isNative())
                methodAccess |= Opcodes.ACC_NATIVE;
            if (method.isPrivate())
                methodAccess |= Opcodes.ACC_PRIVATE;
            if (method.isProtected())
                methodAccess |= Opcodes.ACC_PROTECTED;
            if (method.isPublic())
                methodAccess |= Opcodes.ACC_PUBLIC;
            if (method.isStatic())
                methodAccess |= Opcodes.ACC_STATIC;
            if (method.isStrict())
                methodAccess |= Opcodes.ACC_STRICT;
            if (method.isSynchronized())
                methodAccess |= Opcodes.ACC_SYNCHRONIZED;
            if (method.isVarArgs())
                methodAccess |= Opcodes.ACC_VARARGS;

            /*
             * Aufruf einer neuen MethodSignature
             */
            String methodSignature = null;
            if (method.getSignature() != null)
                methodSignature = buildMethodSignature(method.getSignature());

            Type resultType = Type.getType(method.getDescriptor().getResultType().getTypeDescriptor());
            Type[] argumentTypes = new Type[method.getDescriptor().getParameterTypes().size()];
            for (int i = 0; i < method.getDescriptor().getParameterTypes().size(); i++) {
                argumentTypes[i] = Type.getType(method.getDescriptor().getParameterTypes().get(i).getTypeDescriptor());
            }
            String methodDescriptor = Type.getMethodDescriptor(resultType, argumentTypes);

            
            // TODO why does InternalName omit last letter?
            String[] exceptionTypes = new String[method.getExceptionsCanBeThrown().size()];
            for (int i = 0; i < method.getExceptionsCanBeThrown().size(); i++) {
                exceptionTypes[i] = Type.getType(method.getExceptionsCanBeThrown().get(i).getTypeDescriptor())
                		.toString().substring(1); //.getInternalName()+"n";
            }

            MethodVisitor methodVisitor = classVisitor.visitMethod(methodAccess, method.getName(), methodDescriptor,
                    methodSignature, exceptionTypes);
            assembleMethod(methodVisitor, method);
            // annotations
        }
    }

    private void assembleFields(ClassVisitor classVisitor, Clazz clazz) {
        for (Field field : clazz.getFields()) {
            int fieldAccess = 0;
            if (field.isDeprecated())
                fieldAccess |= Opcodes.ACC_DEPRECATED;
            if (field.isEnum())
                fieldAccess |= Opcodes.ACC_ENUM;
            if (field.isFinal())
                fieldAccess |= Opcodes.ACC_FINAL;
            if (field.isPrivate())
                fieldAccess |= Opcodes.ACC_PRIVATE;
            if (field.isProtected())
                fieldAccess |= Opcodes.ACC_PROTECTED;
            if (field.isPublic())
                fieldAccess |= Opcodes.ACC_PUBLIC;
            if (field.isStatic())
                fieldAccess |= Opcodes.ACC_STATIC;
            if (field.isSynthetic())
                fieldAccess |= Opcodes.ACC_SYNTHETIC;
            if (field.isTransient())
                fieldAccess |= Opcodes.ACC_TRANSIENT;
            if (field.isVolatile())
                fieldAccess |= Opcodes.ACC_VOLATILE;

            /*
             * Aufruf einer FieldTypeSignature
             */
            String fieldSignature = null;
            if (field.getSignature() != null)
                fieldSignature = buildFieldTypeString(field.getSignature());

            FieldVisitor fieldVisitor = classVisitor.visitField(fieldAccess, field.getName(),
                    Type.getType(field.getDescriptor().getTypeDescriptor()).getDescriptor(), fieldSignature,
                    asObjectValue(field.getConstantValue()));

            if (field.getRuntimeInvisibleAnnotations() != null) {
                for (Annotation annot : field.getRuntimeInvisibleAnnotations()) {
                    AnnotationVisitor annotVisitor = fieldVisitor.visitAnnotation(annot.getType().getTypeDescriptor(), false);
                    assembleAnnotation(annotVisitor, annot, clazz.getName() + "." + field.getName());
                }
            }
            if (field.getRuntimeVisibleAnnotations() != null) {
                for (Annotation annot : field.getRuntimeVisibleAnnotations()) {
                    AnnotationVisitor annotVisitor = fieldVisitor.visitAnnotation(annot.getType().getTypeDescriptor(), true);
                    assembleAnnotation(annotVisitor, annot, clazz.getName() + "." + field.getName());
                }
            }

           //  fieldVisitor.visitTypeAnnotation(typeRef, typePath, desc,
           //  visible);
           //  fieldVisitor.visitAttribute(attr);
            fieldVisitor.visitEnd();
        }
    }

    private void assembleAnnotation(AnnotationVisitor annotVisitor, Annotation annot, String location) {
        for (ElementValuePair elementValuePair : annot.getElementValuePairs()) {
            ElementValue annotationValueModel = elementValuePair.getValue();
            if (annotationValueModel.getEnumValue() != null) {
                annotVisitor.visitEnum(elementValuePair.getName(),
                        annotationValueModel.getEnumValue().getType().getTypeDescriptor(),
                        annotationValueModel.getEnumValue().getConstName());
            } else if (annotationValueModel.getAnnotationValue() != null) {
                AnnotationVisitor nestedAnnotVisitor = annotVisitor.visitAnnotation(elementValuePair.getName(),
                        annotationValueModel.getAnnotationValue().getType().getTypeDescriptor());
                assembleAnnotation(nestedAnnotVisitor, annotationValueModel.getAnnotationValue(), location);
            } else if (annotationValueModel.getArrayValue() != null) {
                throw new RuntimeException("Array annotation attributes are currently not supported. (" + location + ")");
            } else {
                Object value;

                if (annotationValueModel.getClassValue() != null) {
                    value = Type.getType(annotationValueModel.getClassValue().getTypeDescriptor());
                } else if (annotationValueModel.getConstantValue() != null) {
                    ElementaryValue elemValue = (ElementaryValue) annotationValueModel.getConstantValue();
                    if (elemValue instanceof BooleanValue) {
                        value = ((BooleanValue) elemValue).isValue();
                    }
                    else if (elemValue instanceof CharValue) {
                        value = ((CharValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof ByteValue) {
                        value = ((ByteValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof ShortValue) {
                        value = ((ShortValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof IntValue) {
                        value = ((IntValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof LongValue) {
                        value = ((LongValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof FloatValue) {
                        value = ((FloatValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof DoubleValue) {
                        value = ((DoubleValue) elemValue).getValue();
                    }
                    else if (elemValue instanceof StringValue) {
                        value = ((StringValue) elemValue).getValue();
                    }
                    else
                        throw new IllegalArgumentException("Unknown type of annotation attribute constant value: " + elemValue.getClass());
                } else
                    throw new IllegalArgumentException("Unknown type of annotation attribute value.");
                annotVisitor.visit(elementValuePair.getName(), value);
            }

        }
        annotVisitor.visitEnd();
    }

    private static final EStructuralFeature LINENUMBER_FEATURE = JbcPackage.eINSTANCE.getInstruction()
            .getEStructuralFeature(JbcPackage.INSTRUCTION__LINENUMBER);

    private void assembleMethod(MethodVisitor methodVisitor, Method method) {
        // methodVisitor.visitParameter(name, access);
        // methodVisitor.visitAnnotationDefault();
        // methodVisitor.visitParameterAnnotation(parameter, desc, visible);
        // methodVisitor.visitAttribute(attr);

        if (method.getRuntimeInvisibleAnnotations() != null) {
            for (Annotation annot : method.getRuntimeInvisibleAnnotations()) {
                AnnotationVisitor annotVisitor = methodVisitor.visitAnnotation(annot.getType().getTypeDescriptor(), false);
                assembleAnnotation(annotVisitor, annot, method.getClass_().getName() + "." + method.getName());
            }
        }
        if (method.getRuntimeVisibleAnnotations() != null) {
            for (Annotation annot : method.getRuntimeVisibleAnnotations()) {
                AnnotationVisitor annotVisitor = methodVisitor.visitAnnotation(annot.getType().getTypeDescriptor(), true);
                assembleAnnotation(annotVisitor, annot, method.getClass_().getName() + "." + method.getName());
            }
        }

        if (!method.isAbstract()) {
            methodVisitor.visitCode();

            Map<Instruction, Label> jumpTargets = new HashMap<>();
            Label terminalLabel = new Label();

            for (ExceptionTableEntry exceptionTableEntry : method.getExceptionTable()) {
                Label startLabel = getLabel(jumpTargets, exceptionTableEntry.getStartInstruction());
                Label endLabel = getLabel(jumpTargets, exceptionTableEntry.getEndInstruction());
                Label handlerLabel = getLabel(jumpTargets, exceptionTableEntry.getHandlerInstruction());
                methodVisitor.visitTryCatchBlock(startLabel, endLabel, handlerLabel,
                        exceptionTableEntry.getCatchType() == null ? null
                                : Type.getType(exceptionTableEntry.getCatchType().getTypeDescriptor())
                                        .getInternalName());
            }
            // methodVisitor.visitTryCatchAnnotation(typeRef, typePath, desc, visible)

            Instruction instruction = method.getFirstInstruction();
            int previousLineNumer = -1;

            while (instruction != null) {

                Label label = getLabel(jumpTargets, instruction);
                methodVisitor.visitLabel(label);

                if (instruction.eIsSet(LINENUMBER_FEATURE) && instruction.getLinenumber() != previousLineNumer) {
                    Label lineNumberStartLabel = new Label();
                    methodVisitor.visitLabel(lineNumberStartLabel);
                    methodVisitor.visitLineNumber(instruction.getLinenumber(), lineNumberStartLabel);
                    previousLineNumer = instruction.getLinenumber();
                }

                if (instruction instanceof FieldInstruction) {
                    int opcode = getOpcode(instruction);
                    FieldInstruction fieldInstruction = (FieldInstruction) instruction;
                    FieldReference fieldReference = fieldInstruction.getFieldReference();
                    methodVisitor.visitFieldInsn(opcode,
                            Type.getType(fieldReference.getDeclaringClass().getTypeDescriptor()).getInternalName(),
                            fieldReference.getName(),
                            Type.getType(fieldReference.getDescriptor().getTypeDescriptor()).getDescriptor());
                } else if (instruction instanceof MethodInstruction) {
                    int opcode = getOpcode(instruction);
                    MethodInstruction methodInstruction = (MethodInstruction) instruction;
                    MethodReference methodReference = methodInstruction.getMethodReference();

                    Type resultType = Type.getType(methodReference.getDescriptor().getResultType().getTypeDescriptor());
                    Type[] argumentTypes = new Type[methodReference.getDescriptor().getParameterTypes().size()];
                    for (int i = 0; i < methodReference.getDescriptor().getParameterTypes().size(); i++) {
                        argumentTypes[i] = Type.getType(
                                methodReference.getDescriptor().getParameterTypes().get(i).getTypeDescriptor());
                    }
                    String methodDescriptor = Type.getMethodDescriptor(resultType, argumentTypes);

                    if (getOpcode(methodInstruction) == Opcodes.INVOKEDYNAMIC) {
                        throw new RuntimeException("unsupported instruction " + methodInstruction);
                    } else {
                        methodVisitor.visitMethodInsn(opcode,
                                Type.getType(methodReference.getDeclaringClass().getTypeDescriptor()).getInternalName(),
                                methodReference.getName(), methodDescriptor);
                    }
                } else if (instruction instanceof TypeInstruction) {
                    int opcode = getOpcode(instruction);
                    TypeInstruction typeInstruction = (TypeInstruction) instruction;
                    methodVisitor.visitTypeInsn(opcode,
                            Type.getType(typeInstruction.getTypeReference().getTypeDescriptor()).getInternalName());
                } else if (instruction instanceof MultianewarrayInstruction) {
                    MultianewarrayInstruction multianewarrayInstruction = (MultianewarrayInstruction) instruction;
                    Type arrayType = Type.getType(multianewarrayInstruction.getTypeReference().getTypeDescriptor());
                    methodVisitor.visitMultiANewArrayInsn(arrayType.getDescriptor(), arrayType.getDimensions());
                } else if (instruction instanceof SwitchInstruction) {
                    SwitchInstruction switchInstruction = (SwitchInstruction) instruction;
                    Label defaultLabel = null;
                    int minimum = 0;
                    boolean canUseTable = true;
                    List<Label> caseLabelsList = new ArrayList<>();
                    List<Integer> keysList = new ArrayList<>();
                    for (ControlFlowEdge edge : switchInstruction.getOutEdges()) {
                        if (edge instanceof SwitchDefaultEdge) {
                            defaultLabel = getLabel(jumpTargets, edge.getEnd());
                        } else if (edge instanceof SwitchCaseEdge) {
                            if (caseLabelsList.isEmpty()) {
                                minimum = ((SwitchCaseEdge) edge).getCondition();
                            }
                            Label caseLabel = getLabel(jumpTargets, edge.getEnd());
                            caseLabelsList.add(caseLabel);
                            keysList.add(((SwitchCaseEdge) edge).getCondition());
                            if (minimum + caseLabelsList.size() - 1 != ((SwitchCaseEdge) edge).getCondition())
                                canUseTable = false;
                        }
                    }
                    Label[] caseLabels = new Label[caseLabelsList.size()];
                    caseLabels = caseLabelsList.toArray(caseLabels);
                    if (canUseTable) {
                        methodVisitor.visitTableSwitchInsn(minimum, minimum + caseLabelsList.size() - 1, defaultLabel,
                                caseLabels);
                    } else {
                        int[] keys = new int[keysList.size()];
                        for (int i = 0; i < keysList.size(); i++) {
                            keys[i] = keysList.get(i);
                        }
                        methodVisitor.visitLookupSwitchInsn(defaultLabel, keys, caseLabels);
                    }
                } else if (instruction instanceof LdcIntInstruction) {
                    methodVisitor.visitLdcInsn(((LdcIntInstruction) instruction).getConstant());
                } else if (instruction instanceof LdcLongInstruction) {
                    methodVisitor.visitLdcInsn(((LdcLongInstruction) instruction).getConstant());
                } else if (instruction instanceof LdcFloatInstruction) {
                    methodVisitor.visitLdcInsn(((LdcFloatInstruction) instruction).getConstant());
                } else if (instruction instanceof LdcDoubleInstruction) {
                    methodVisitor.visitLdcInsn(((LdcDoubleInstruction) instruction).getConstant());
                } else if (instruction instanceof LdcStringInstruction) {
                    String stringConstant = ((LdcStringInstruction) instruction).getConstant();
                    StringWriter writer = new StringWriter(stringConstant.length());
                    try {
                        StringEscapeUtils.unescapeJava(writer, stringConstant);
                        methodVisitor.visitLdcInsn(writer.toString());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                } else if (instruction instanceof LdcTypeInstruction) {
                    methodVisitor.visitLdcInsn(
                            Type.getType(((LdcTypeInstruction) instruction).getConstant().getTypeDescriptor()));
                } else if (instruction instanceof IntInstruction) {
                    int opcode = getOpcode(instruction);
                    methodVisitor.visitIntInsn(opcode, ((IntInstruction) instruction).getOperand());
                } else if (instruction instanceof JumpInstruction) {
                    int opcode = getOpcode(instruction);
                    Label target = null;
                    for (ControlFlowEdge edge : ((JumpInstruction) instruction).getOutEdges()) {
                        if (edge instanceof ConditionalEdge && ((ConditionalEdge) edge).isCondition() == true) {
                            target = getLabel(jumpTargets, edge.getEnd());
                        } else if (edge instanceof UnconditionalEdge
                                && (instruction instanceof GotoInstruction || instruction instanceof JsrInstruction)) {
                            target = getLabel(jumpTargets, edge.getEnd());
                        }
                    }
                    methodVisitor.visitJumpInsn(opcode, target);
                } else if (instruction instanceof VarInstruction) {
                    int opcode = getOpcode(instruction);
                    if (instruction instanceof IincInstruction) {
                        IincInstruction iincInstruction = (IincInstruction) instruction;
                        methodVisitor.visitIincInsn(iincInstruction.getVarIndex(), iincInstruction.getIncr());
                    } else {
                        methodVisitor.visitVarInsn(opcode, ((VarInstruction) instruction).getVarIndex());
                    }
                } else {
                    int opcode = getOpcode(instruction);
                    methodVisitor.visitInsn(opcode);
                }

                instruction = getNextInCodeOrder(instruction);
            }
            methodVisitor.visitLabel(terminalLabel);

            for (LocalVariableTableEntry entry : method.getLocalVariableTable()) {
            	Instruction endInstruction = getNextInCodeOrder(entry.getEndInstruction());
                Label endLabel;
                if (endInstruction == null) {
                    endLabel = terminalLabel;
                }
                else {
                    endLabel = jumpTargets.get(endInstruction);
                }
                
                // SIGNATURE STUFF 02/08/2022
                String signatureString = null;
                if(entry.getSignature() instanceof TypeVariableSignature tvs) {
                	signatureString = "T"+tvs.getIdentifier()+";";
                } 
                
                else if(entry.getSignature() instanceof ClassTypeSignature cts) {
                	EList<TypeArgument> tempTAs = cts.getSimpleClassSignature().getTypeArgument();
                	
                	signatureString = "L"+cts.getPackagespecifier()+
                			"/"+cts.getSimpleClassSignature().getIdentifier();
                	
                	if(!tempTAs.isEmpty()) signatureString+= "<";
                	
                	for(TypeArgument ta: tempTAs) {
                		if(ta instanceof TypeConcrete tc) {
                			
                			if(tc.getFieldTypeSignature() instanceof TypeVariableSignature tvs) {
                				signatureString+= "T"+ tvs.getIdentifier()+ ";";
                			}
                			
                			else if(tc.getFieldTypeSignature() instanceof ClassTypeSignature ctsInner) {
                				signatureString+= "L"+ 
                				ctsInner.getPackagespecifier() + "/" +
                				ctsInner.getSimpleClassSignature().getIdentifier()+";";
                			}
                		}
                	}
                	if(!tempTAs.isEmpty()) signatureString+= ">";

                	signatureString += ";";
                }
                /////
                
                methodVisitor.visitLocalVariable(entry.getName(),
                        Type.getType(entry.getDescriptor().getTypeDescriptor()).getDescriptor(),
                        signatureString,   /// WAR VORHER null
                        jumpTargets.get(entry.getStartInstruction()), 
                        endLabel, 
                        entry.getIndex());
            }

            methodVisitor.visitMaxs(0, 0);
        }   
        methodVisitor.visitEnd();

    }

    private Instruction getNextInCodeOrder(Instruction instruction) {
        if (instruction instanceof SwitchInstruction) {
            return instruction.getOutEdges().get(0).getEnd();
        } else if (instruction instanceof ReturnInstruction || instruction instanceof IreturnInstruction
                || instruction instanceof LreturnInstruction || instruction instanceof FreturnInstruction
                || instruction instanceof DreturnInstruction || instruction instanceof AreturnInstruction
                || instruction instanceof AthrowInstruction || instruction instanceof RetInstruction) {
            for (ControlFlowEdge edge : instruction.getOutEdges()) {
                if (edge instanceof ResumeEdge) {
                    return edge.getEnd();
                }
            }
            // if no resume edge was found, the instruction must be the last in the code order
            return null;
        } else if (instruction instanceof GotoInstruction || instruction instanceof JsrInstruction) {
            for (ControlFlowEdge edge : instruction.getOutEdges()) {
                if (edge instanceof ResumeEdge) {
                    return edge.getEnd();
                }
            }
        } else if (instruction instanceof JumpInstruction) {
            for (ControlFlowEdge edge : instruction.getOutEdges()) {
                if (edge instanceof ConditionalEdge) {
                    if (((ConditionalEdge) edge).isCondition() == false) {
                        return edge.getEnd();
                    }
                }
            }
        } else {
            for (ControlFlowEdge edge : instruction.getOutEdges()) {
                if (edge instanceof UnconditionalEdge)
                    return ((UnconditionalEdge) edge).getEnd();
                if (edge instanceof ResumeEdge)
                    return ((UnconditionalEdge) edge).getEnd();
            }
        }
        throw new RuntimeException("Cannot determine next in code order");
    }

    private Label getLabel(Map<Instruction, Label> jumpTargets, Instruction instruction) {
        Label label = jumpTargets.get(instruction);
        if (label == null) {
            label = new Label();
            jumpTargets.put(instruction, label);
        }
        return label;
    }

    private int getOpcode(Instruction instruction) {
        return InstructionToOpcode.get(instruction);
    }

    // private void createSequence(Instruction firstInstruction, List<Instruction>
    // instructionSequence,
    // Map<Instruction, Label> jumpTargets) {
    //
    // Deque<Instruction> nextInstructions = new ArrayDeque<>();
    //
    // nextInstructions.push(firstInstruction);
    //
    // while (!nextInstructions.isEmpty()) {
    // Instruction currentInstruction = nextInstructions.pop();
    // if (instructionSequence.contains(currentInstruction))
    // continue;
    //
    // instructionSequence.add(currentInstruction);
    //
    // List<ControlFlowEdge> outEdges = currentInstruction.getOutEdges();
    // if (outEdges.isEmpty()) {
    // continue;
    // }
    // else {
    // ControlFlowEdge unconditionalEdge = null;
    // List<ControlFlowEdge> exceptionalEdges = new ArrayList<>();
    // List<ControlFlowEdge> conditionalEdges = new ArrayList<>();
    // List<ControlFlowEdge> switchCaseEdges = new ArrayList<>();
    // ControlFlowEdge switchDefaultEdge = null;
    // for (ControlFlowEdge edge : currentInstruction.getOutEdges()) {
    // if (edge instanceof UnconditionalEdge)
    // unconditionalEdge = edge;
    // else if (edge instanceof ExceptionalEdge)
    // exceptionalEdges.add(edge);
    // else if (edge instanceof ConditionalEdge)
    // conditionalEdges.add(edge);
    // else if (edge instanceof SwitchCaseEdge)
    // switchCaseEdges.add(edge);
    // else if (edge instanceof SwitchDefaultEdge)
    // switchDefaultEdge = edge;
    // else
    // throw new RuntimeException("Unsupported control flow edge " + edge);
    // }
    //
    // for (ControlFlowEdge controlFlowEdge : exceptionalEdges) {
    // nextInstructions.push(controlFlowEdge.getEnd());
    // jumpTargets.put(controlFlowEdge.getEnd(), null);
    // }
    // if (switchDefaultEdge != null) {
    // nextInstructions.push(switchDefaultEdge.getEnd());
    // jumpTargets.put(switchDefaultEdge.getEnd(), null);
    // }
    // Collections.reverse(switchCaseEdges);
    // for (ControlFlowEdge controlFlowEdge : switchCaseEdges) {
    // nextInstructions.push(controlFlowEdge.getEnd());
    // jumpTargets.put(controlFlowEdge.getEnd(), null);
    // }
    // for (ControlFlowEdge controlFlowEdge : conditionalEdges) {
    // nextInstructions.push(controlFlowEdge.getEnd());
    // jumpTargets.put(controlFlowEdge.getEnd(), null);
    // }
    // if (currentInstruction instanceof GotoInstruction || currentInstruction
    // instanceof JsrInstruction) {
    // nextInstructions.addFirst(unconditionalEdge.getEnd());
    // jumpTargets.put(unconditionalEdge.getEnd(), null);
    // } else {
    // if (unconditionalEdge != null)
    // nextInstructions.push(unconditionalEdge.getEnd());
    // }
    // }
    // }
    // }

    private Object asObjectValue(ElementaryValue value) {
        if (value == null)
            return null;
        else {
            if (value instanceof BooleanValue)
                return ((BooleanValue) value).isValue();
            else if (value instanceof CharValue) {
                String escaped = ((CharValue) value).getValue();
                if (escaped.equals("\\\'"))
                    return '\'';
                else
                    return StringEscapeUtils.unescapeJava(escaped).charAt(0);
            } else if (value instanceof ByteValue)
                return ((ByteValue) value).getValue();
            else if (value instanceof ShortValue)
                return ((ShortValue) value).getValue();
            else if (value instanceof IntValue)
                return ((IntValue) value).getValue();
            else if (value instanceof LongValue)
                return ((LongValue) value).getValue();
            else if (value instanceof FloatValue)
                return ((FloatValue) value).getValue();
            else if (value instanceof DoubleValue)
                return ((DoubleValue) value).getValue();
            else if (value instanceof StringValue) {
                String escaped = ((StringValue) value).getValue();
                return StringEscapeUtils.unescapeJava(escaped);
            } else
                throw new RuntimeException("Unsupported elementary value " + value);
        }
    }

}
