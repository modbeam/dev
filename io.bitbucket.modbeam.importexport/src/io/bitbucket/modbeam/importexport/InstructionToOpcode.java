package io.bitbucket.modbeam.importexport;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.util.Printer;

import io.bitbucket.modbeam.jbc.*;

public class InstructionToOpcode {

    private static final Map<String, Integer> MNEMONIC_TO_OPCODE = new HashMap<>();

    static {
        for (int i = 0; i < Printer.OPCODES.length; i++) {
            if (!Printer.OPCODES[i].equals(""))
                MNEMONIC_TO_OPCODE.put(Printer.OPCODES[i], i);
        }
    }

    public static int get(Instruction instruction) {
        return MNEMONIC_TO_OPCODE.get(getMnemonic(instruction));
    }

    /**
     * Returns the Mnemonic for the instruction as used by ASM.
     * For the SwitchInstruction this method returns "SWITCH", which should only
     * be used for informational purposes, as usually a further differentiation between
     * a table and lookup switch must be made. 
     * @param instruction
     * @return
     */
    public static String getLogiclMnemonic(Instruction instruction) {
        if (instruction instanceof SwitchInstruction) {
            return "SWITCH";
        }
        else {
            return getMnemonic(instruction);
        }
    }

    /**
     * Returns the Mnemonic for the instruction as used by ASM.
     * This method cannot be used for the SwitchInstruction because this does not
     * differentiate between TABLE_SWITCH and LOOKUP_SWITCH as needed by ASM. 
     * @param instruction
     * @return
     */
    public static String getMnemonic(Instruction instruction) {
        if (instruction instanceof AaloadInstruction) {
            return "AALOAD";
        }
        if (instruction instanceof AastoreInstruction) {
            return "AASTORE";
        }
        if (instruction instanceof Aconst_nullInstruction) {
            return "ACONST_NULL";
        }
        if (instruction instanceof AloadInstruction) {
            return "ALOAD";
        }
        if (instruction instanceof AnewarrayInstruction) {
            return "ANEWARRAY";
        }
        if (instruction instanceof AreturnInstruction) {
            return "ARETURN";
        }
        if (instruction instanceof ArraylengthInstruction) {
            return "ARRAYLENGTH";
        }
        if (instruction instanceof AstoreInstruction) {
            return "ASTORE";
        }
        if (instruction instanceof AthrowInstruction) {
            return "ATHROW";
        }
        if (instruction instanceof BaloadInstruction) {
            return "BALOAD";
        }
        if (instruction instanceof BastoreInstruction) {
            return "BASTORE";
        }
        if (instruction instanceof BipushInstruction) {
            return "BIPUSH";
        }
        if (instruction instanceof CaloadInstruction) {
            return "CALOAD";
        }
        if (instruction instanceof CastoreInstruction) {
            return "CASTORE";
        }
        if (instruction instanceof CheckcastInstruction) {
            return "CHECKCAST";
        }
        if (instruction instanceof D2fInstruction) {
            return "D2F";
        }
        if (instruction instanceof D2iInstruction) {
            return "D2I";
        }
        if (instruction instanceof D2lInstruction) {
            return "D2L";
        }
        if (instruction instanceof DaddInstruction) {
            return "DADD";
        }
        if (instruction instanceof DaloadInstruction) {
            return "DALOAD";
        }
        if (instruction instanceof DastoreInstruction) {
            return "DASTORE";
        }
        if (instruction instanceof DcmpgInstruction) {
            return "DCMPG";
        }
        if (instruction instanceof DcmplInstruction) {
            return "DCMPL";
        }
        if (instruction instanceof Dconst_0Instruction) {
            return "DCONST_0";
        }
        if (instruction instanceof Dconst_1Instruction) {
            return "DCONST_1";
        }
        if (instruction instanceof DdivInstruction) {
            return "DDIV";
        }
        if (instruction instanceof DloadInstruction) {
            return "DLOAD";
        }
        if (instruction instanceof DmulInstruction) {
            return "DMUL";
        }
        if (instruction instanceof DnegInstruction) {
            return "DNEG";
        }
        if (instruction instanceof DremInstruction) {
            return "DREM";
        }
        if (instruction instanceof DreturnInstruction) {
            return "DRETURN";
        }
        if (instruction instanceof DstoreInstruction) {
            return "DSTORE";
        }
        if (instruction instanceof DsubInstruction) {
            return "DSUB";
        }
        if (instruction instanceof Dup2_x1Instruction) {
            return "DUP2_X1";
        }
        if (instruction instanceof Dup2_x2Instruction) {
            return "DUP2_X2";
        }
        if (instruction instanceof Dup2Instruction) {
            return "DUP2";
        }
        if (instruction instanceof Dup_x1Instruction) {
            return "DUP_X1";
        }
        if (instruction instanceof Dup_x2Instruction) {
            return "DUP_X2";
        }
        if (instruction instanceof DupInstruction) {
            return "DUP";
        }
        if (instruction instanceof F2dInstruction) {
            return "F2D";
        }
        if (instruction instanceof F2iInstruction) {
            return "F2I";
        }
        if (instruction instanceof F2lInstruction) {
            return "F2L";
        }
        if (instruction instanceof FaddInstruction) {
            return "FADD";
        }
        if (instruction instanceof FaloadInstruction) {
            return "FALOAD";
        }
        if (instruction instanceof FastoreInstruction) {
            return "FASTORE";
        }
        if (instruction instanceof FcmpgInstruction) {
            return "FCMPG";
        }
        if (instruction instanceof FcmplInstruction) {
            return "FCMPL";
        }
        if (instruction instanceof Fconst_0Instruction) {
            return "FCONST_0";
        }
        if (instruction instanceof Fconst_1Instruction) {
            return "FCONST_1";
        }
        if (instruction instanceof Fconst_2Instruction) {
            return "FCONST_2";
        }
        if (instruction instanceof FdivInstruction) {
            return "FDIV";
        }
        if (instruction instanceof FloadInstruction) {
            return "FLOAD";
        }
        if (instruction instanceof FmulInstruction) {
            return "FMUL";
        }
        if (instruction instanceof FnegInstruction) {
            return "FNEG";
        }
        if (instruction instanceof FremInstruction) {
            return "FREM";
        }
        if (instruction instanceof FreturnInstruction) {
            return "FRETURN";
        }
        if (instruction instanceof FstoreInstruction) {
            return "FSTORE";
        }
        if (instruction instanceof FsubInstruction) {
            return "FSUB";
        }
        if (instruction instanceof GetfieldInstruction) {
            return "GETFIELD";
        }
        if (instruction instanceof GetstaticInstruction) {
            return "GETSTATIC";
        }
        if (instruction instanceof GotoInstruction) {
            return "GOTO";
        }
        if (instruction instanceof I2bInstruction) {
            return "I2B";
        }
        if (instruction instanceof I2cInstruction) {
            return "I2C";
        }
        if (instruction instanceof I2dInstruction) {
            return "I2D";
        }
        if (instruction instanceof I2fInstruction) {
            return "I2F";
        }
        if (instruction instanceof I2lInstruction) {
            return "I2L";
        }
        if (instruction instanceof I2sInstruction) {
            return "I2S";
        }
        if (instruction instanceof IaddInstruction) {
            return "IADD";
        }
        if (instruction instanceof IaloadInstruction) {
            return "IALOAD";
        }
        if (instruction instanceof IandInstruction) {
            return "IAND";
        }
        if (instruction instanceof IastoreInstruction) {
            return "IASTORE";
        }
        if (instruction instanceof Iconst_0Instruction) {
            return "ICONST_0";
        }
        if (instruction instanceof Iconst_1Instruction) {
            return "ICONST_1";
        }
        if (instruction instanceof Iconst_2Instruction) {
            return "ICONST_2";
        }
        if (instruction instanceof Iconst_3Instruction) {
            return "ICONST_3";
        }
        if (instruction instanceof Iconst_4Instruction) {
            return "ICONST_4";
        }
        if (instruction instanceof Iconst_5Instruction) {
            return "ICONST_5";
        }
        if (instruction instanceof Iconst_m1Instruction) {
            return "ICONST_M1";
        }
        if (instruction instanceof IdivInstruction) {
            return "IDIV";
        }
        if (instruction instanceof If_acmpeqInstruction) {
            return "IF_ACMPEQ";
        }
        if (instruction instanceof If_acmpneInstruction) {
            return "IF_ACMPNE";
        }
        if (instruction instanceof If_icmpeqInstruction) {
            return "IF_ICMPEQ";
        }
        if (instruction instanceof If_icmpgeInstruction) {
            return "IF_ICMPGE";
        }
        if (instruction instanceof If_icmpgtInstruction) {
            return "IF_ICMPGT";
        }
        if (instruction instanceof If_icmpleInstruction) {
            return "IF_ICMPLE";
        }
        if (instruction instanceof If_icmpltInstruction) {
            return "IF_ICMPLT";
        }
        if (instruction instanceof If_icmpneInstruction) {
            return "IF_ICMPNE";
        }
        if (instruction instanceof IfeqInstruction) {
            return "IFEQ";
        }
        if (instruction instanceof IfgeInstruction) {
            return "IFGE";
        }
        if (instruction instanceof IfgtInstruction) {
            return "IFGT";
        }
        if (instruction instanceof IfleInstruction) {
            return "IFLE";
        }
        if (instruction instanceof IfltInstruction) {
            return "IFLT";
        }
        if (instruction instanceof IfneInstruction) {
            return "IFNE";
        }
        if (instruction instanceof IfnonnullInstruction) {
            return "IFNONNULL";
        }
        if (instruction instanceof IfnullInstruction) {
            return "IFNULL";
        }
        if (instruction instanceof IincInstruction) {
            return "IINC";
        }
        if (instruction instanceof IloadInstruction) {
            return "ILOAD";
        }
        if (instruction instanceof ImulInstruction) {
            return "IMUL";
        }
        if (instruction instanceof InegInstruction) {
            return "INEG";
        }
        if (instruction instanceof InstanceofInstruction) {
            return "INSTANCEOF";
        }
        if (instruction instanceof InvokeinterfaceInstruction) {
            return "INVOKEINTERFACE";
        }
        if (instruction instanceof InvokespecialInstruction) {
            return "INVOKESPECIAL";
        }
        if (instruction instanceof InvokestaticInstruction) {
            return "INVOKESTATIC";
        }
        if (instruction instanceof InvokevirtualInstruction) {
            return "INVOKEVIRTUAL";
        }
        if (instruction instanceof IorInstruction) {
            return "IOR";
        }
        if (instruction instanceof IremInstruction) {
            return "IREM";
        }
        if (instruction instanceof IreturnInstruction) {
            return "IRETURN";
        }
        if (instruction instanceof IshlInstruction) {
            return "ISHL";
        }
        if (instruction instanceof IshrInstruction) {
            return "ISHR";
        }
        if (instruction instanceof IstoreInstruction) {
            return "ISTORE";
        }
        if (instruction instanceof IsubInstruction) {
            return "ISUB";
        }
        if (instruction instanceof IushrInstruction) {
            return "IUSHR";
        }
        if (instruction instanceof IxorInstruction) {
            return "IXOR";
        }
        if (instruction instanceof JsrInstruction) {
            return "JSR";
        }
        if (instruction instanceof L2dInstruction) {
            return "L2D";
        }
        if (instruction instanceof L2fInstruction) {
            return "L2F";
        }
        if (instruction instanceof L2iInstruction) {
            return "L2I";
        }
        if (instruction instanceof LaddInstruction) {
            return "LADD";
        }
        if (instruction instanceof LaloadInstruction) {
            return "LALOAD";
        }
        if (instruction instanceof LandInstruction) {
            return "LAND";
        }
        if (instruction instanceof LastoreInstruction) {
            return "LASTORE";
        }
        if (instruction instanceof LcmpInstruction) {
            return "LCMP";
        }
        if (instruction instanceof Lconst_0Instruction) {
            return "LCONST_0";
        }
        if (instruction instanceof Lconst_1Instruction) {
            return "LCONST_1";
        }
        if (instruction instanceof LdcInstruction) {
            return "LDC";
        }
        if (instruction instanceof LdivInstruction) {
            return "LDIV";
        }
        if (instruction instanceof LloadInstruction) {
            return "LLOAD";
        }
        if (instruction instanceof LmulInstruction) {
            return "LMUL";
        }
        if (instruction instanceof LnegInstruction) {
            return "LNEG";
        }
        if (instruction instanceof LorInstruction) {
            return "LOR";
        }
        if (instruction instanceof LremInstruction) {
            return "LREM";
        }
        if (instruction instanceof LreturnInstruction) {
            return "LRETURN";
        }
        if (instruction instanceof LshlInstruction) {
            return "LSHL";
        }
        if (instruction instanceof LshrInstruction) {
            return "LSHR";
        }
        if (instruction instanceof LstoreInstruction) {
            return "LSTORE";
        }
        if (instruction instanceof LsubInstruction) {
            return "LSUB";
        }
        if (instruction instanceof LushrInstruction) {
            return "LUSHR";
        }
        if (instruction instanceof LxorInstruction) {
            return "LXOR";
        }
        if (instruction instanceof MonitorenterInstruction) {
            return "MONITORENTER";
        }
        if (instruction instanceof MonitorexitInstruction) {
            return "MONITOREXIT";
        }
        if (instruction instanceof MultianewarrayInstruction) {
            return "MULTIANEWARRAY";
        }
        if (instruction instanceof NewarrayInstruction) {
            return "NEWARRAY";
        }
        if (instruction instanceof NewInstruction) {
            return "NEW";
        }
        if (instruction instanceof NopInstruction) {
            return "NOP";
        }
        if (instruction instanceof Pop2Instruction) {
            return "POP2";
        }
        if (instruction instanceof PopInstruction) {
            return "POP";
        }
        if (instruction instanceof PutfieldInstruction) {
            return "PUTFIELD";
        }
        if (instruction instanceof PutstaticInstruction) {
            return "PUTSTATIC";
        }
        if (instruction instanceof RetInstruction) {
            return "RET";
        }
        if (instruction instanceof ReturnInstruction) {
            return "RETURN";
        }
        if (instruction instanceof SaloadInstruction) {
            return "SALOAD";
        }
        if (instruction instanceof SastoreInstruction) {
            return "SASTORE";
        }
        if (instruction instanceof SipushInstruction) {
            return "SIPUSH";
        }
        if (instruction instanceof SwapInstruction) {
            return "SWAP";
        }
        if (instruction instanceof SwitchInstruction) {
            throw new RuntimeException("This method cannot be used for the SwitchInstruction because this does not "
                    + "differentiate between TABLE_SWITCH and LOOKUP_SWITCH as needed by ASM.");
        }
        throw new RuntimeException("Cannot find mnemonic for " + instruction);
    }

}
