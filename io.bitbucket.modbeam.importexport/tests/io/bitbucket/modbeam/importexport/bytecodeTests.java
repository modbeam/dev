package io.bitbucket.modbeam.importexport;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.ProcessBuilder.Redirect;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.text.diff.StringsComparator;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import io.bitbucket.modbeam.jbc.Clazz;

class bytecodeTests {
	
	java.util.logging.Logger logger =  java.util.logging.Logger.getLogger(this.getClass().getName());
	
	// Gets all the .class Files out of "\bin\\..." for the parameterised Test
	private static String[] getSource() {
		File folder = new File("bin\\io\\bitbucket\\modbeam\\importexport\\");
		File[] listOfFiles = folder.listFiles();

		String[] result = new String[listOfFiles.length-6];
		
		int i = 0;
		for (File file : listOfFiles) {
		    if (file.isFile() && 
		    		!file.getName().contains("Exporter") &&
		    		!file.getName().contains("Extractor") &&
		    		!file.getName().contains("InstructionToOpcode") &&
		    		!file.getName().contains("bytecodeTests") &&
		    		!file.getName().contains("FileCommandsVisitor") &&
		    		!file.getName().contains("MyCommandsVisitor")){
		        //System.out.println(file.getName());
		    	result[i] = file.getPath();
		    	i++;
		    }
		}	
		return result;
	}

	// Old Code, unsure if needed again
	@SuppressWarnings("unused")
	private static boolean compare(String classFile1, String classFile2) throws IOException, InterruptedException {
		Process proc = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
//				.command("javap", "-p", "-c", "-constants", classFile1).start();
		.command("javap", "-p", "-c", "-v", "-constants", classFile1).start();
		
		Process proc2 = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
				.command("javap", "-p", "-c", "-constants", classFile2).start();
		
		String result = new String(proc.getInputStream().readAllBytes()).replaceAll("#[0-9]*", "##");
		String result2 = new String(proc2.getInputStream().readAllBytes()).replaceAll("#[0-9]*", "##");
		
		proc.waitFor();
		proc2.waitFor();

		boolean equals = (result.equals(result2));
		
		return equals;
    }
	
	// Gets the Content of a .class File as a String
	private static String getClassCodeText(String classFile) throws IOException, InterruptedException {
		Process proc = new ProcessBuilder().directory(new File("."))
				.redirectError(Redirect.INHERIT)
				.redirectInput(Redirect.INHERIT)
//				.command("javap", "-p", "-c", "-constants", classFile).start();
				.command("javap", "-p", "-c", "-v", "-constants", classFile).start();
		
		String result = new String(proc.getInputStream().readAllBytes()).replaceAll("#[0-9|| ]*", "## ");

		proc.waitFor();
		
		return result.substring(result.indexOf("{"), result.lastIndexOf("}")+1);
	}
	
	// Compares the Content of two .class Files and creates a Diff.html showcasing where they differ
	private void createDiff(String classFile1, String classFile2) throws IOException, InterruptedException {
		
		Files.write(Paths.get("Output\\Text1.txt"), getClassCodeText(classFile1).getBytes());
		Files.write(Paths.get("Output\\Text2.txt"), getClassCodeText(classFile2).getBytes());
		
		LineIterator file1 = FileUtils.lineIterator(new File("Output\\Text1.txt"), "utf-8");
		LineIterator file2 = FileUtils.lineIterator(new File("Output\\Text2.txt"), "utf-8");
 				
		FileCommandsVisitor fileCommandsVisitor = new FileCommandsVisitor();
 
		while (file1.hasNext() || file2.hasNext()) {

			String left = (file1.hasNext() ? file1.nextLine() : "") + "\n";
			String right = (file2.hasNext() ? file2.nextLine() : "") + "\n";
			
			StringsComparator comparator = new StringsComparator(left, right);
 
			if (comparator.getScript().getLCSLength() > (Integer.max(left.length(), right.length()) * 0.4)) {
				comparator.getScript().visit(fileCommandsVisitor);
			} else {
				StringsComparator leftComparator = new StringsComparator(left, "\n");
				leftComparator.getScript().visit(fileCommandsVisitor);
				StringsComparator rightComparator = new StringsComparator("\n", right);
				rightComparator.getScript().visit(fileCommandsVisitor);
			}
		}
 
		fileCommandsVisitor.generateHTML(classFile1);
	}
			
	/* The Test(s):
	* Takes the .class Files out of "bin\\...", creates their respective .class files that result from the 
	* Meta-Model transformation, then compares the two.
	* If they differ, the Test fails, and provides a Diff.html
	*/
	@ParameterizedTest
	@MethodSource("getSource")
	void diffTest(String testClass) throws IOException, InterruptedException {
		FileInputStream fin = new FileInputStream(testClass);
		Clazz ex = new Extractor().extract(fin);
		new Exporter().export(ex, "Output");
		
		String outPath = "Output/io/bitbucket/modbeam/importexport/" + testClass.substring(38)
		.replace('/', File.separatorChar);
				
		String original = getClassCodeText(testClass);
		String transformed = getClassCodeText(outPath);
		
		// If they are 100% equal Strings, we do not need a Diff
		//boolean equals = compare(testClass, outPath);
		boolean equals = original.equals(transformed);
		boolean fp = equals ? true : falsePositiveCheck(original,transformed);
		
		if(!equals&&!fp) {
			createDiff(testClass, outPath);
		}
		if(!equals && fp) logger.info("FALSE POS: "+ testClass.substring(38));
		assertTrue(equals||fp);
	}

	private boolean falsePositiveCheck(String s1, String s2) throws IOException {
		boolean result = true;
		
		StringReader reader = new StringReader(s1);
	    BufferedReader br = new BufferedReader(reader);
	    String line;
	    
	    while((line=br.readLine())!=null)
	    {
	    	if(!s2.contains(line)){
	    		System.out.println(":1:"+line);
	    		result = false;
	    	   }
	    }
	    
	    reader = new StringReader(s2);
	    br = new BufferedReader(reader);
	    
	    while((line=br.readLine())!=null)
	    {
	    	if(!s1.contains(line)){
	    		System.out.println(":2:"+line);
	    		result = false;
	    	   }
	    }
		
		return result;
	}

}
