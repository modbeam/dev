package io.bitbucket.modbeam.importexport;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.text.diff.CommandVisitor;

class FileCommandsVisitor implements CommandVisitor<Character> {
	 
	private static final String DELETION = "<span style=\"background-color: #ED5858\">${text}</span>";
	private static final String INSERTION = "<span style=\"background-color: #45EA85\">${text}</span>";
 
	//private static final String DELETION = "${text}</span>";
	//private static final String INSERTION = "${text}</span>";
	
	private String left = "";
	private String right = "";
 
	@Override
	public void visitKeepCommand(Character c) {
		String toAppend = "\n".equals("" + c) ? "<br/>" : "" + c;
		left = left + toAppend;
		right = right + toAppend;
	}
 
	@Override
	public void visitInsertCommand(Character c) {
		String toAppend = "\n".equals("" + c) ? "<br/>" : "" + c;
		right = right + INSERTION.replace("${text}", "" + toAppend);
	}
 
	@Override
	public void visitDeleteCommand(Character c) {
		String toAppend = "\n".equals("" + c) ? "<br/>" : "" + c;
		left = left + DELETION.replace("${text}", "" + toAppend);
	}
 
	public void generateHTML(String classFile) throws IOException {
 
		String outName = "Output\\Diff\\"+classFile.substring(38)+".html";
		
		String template = FileUtils.readFileToString(new File("Output\\difftemplate.html"), "utf-8");
		String out1 = template.replace("${left}", left);
		String output = out1.replace("${right}", right);
		FileUtils.write(new File(outName), output, "utf-8");
		//System.out.println("HTML diff for "+ classFile.substring(38) +" generated.");
	}
}
