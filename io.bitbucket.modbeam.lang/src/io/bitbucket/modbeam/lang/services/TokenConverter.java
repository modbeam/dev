package io.bitbucket.modbeam.lang.services;
import org.eclipse.xtext.common.services.Ecore2XtextTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter;
import org.eclipse.xtext.conversion.impl.STRINGValueConverter;
import org.eclipse.xtext.nodemodel.INode;

public class TokenConverter extends Ecore2XtextTerminalConverters {
	
	@ValueConverter(rule = "EString")
	public IValueConverter<String> EString() {
		return new AbstractNullSafeConverter<String>() {
			@Override
			protected String internalToValue(String string, INode node) {
				return STRING().toValue(string, node);
			}

			@Override
			protected String internalToString(String value) {
				return STRING().toString(value);
			}
		};
	}
	
    @ValueConverter(rule = "NAME")
    public IValueConverter<String> NAME() {
        return new AbstractNullSafeConverter<String>() {
            @Override
            protected String internalToValue(String string, INode node) {
                return ID().toValue(string, node);
            }

            @Override
            protected String internalToString(String value) {
                try {
                    return ID().toString(value);
                }
                catch (ValueConverterException e) {
                    return value;
                }
            }
        };
    }

    @ValueConverter(rule = "METHODNAME")
    public IValueConverter<String> METHODNAME() {
        return new AbstractNullSafeConverter<String>() {
            @Override
            protected String internalToValue(String string, INode node) {
                return ID().toValue(string, node);
            }

            @Override
            protected String internalToString(String value) {
                if (value.equals("<init>") || value.equals("<clinit>"))
                    return value;
                return ID().toString(value);
            }
        };
    }
	
}