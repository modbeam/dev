/*
 * generated by Xtext 2.24.0
 */
package io.bitbucket.modbeam.lang.parser.antlr;

import com.google.inject.Inject;
import io.bitbucket.modbeam.lang.parser.antlr.internal.InternalJBCParser;
import io.bitbucket.modbeam.lang.services.JBCGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class JBCParser extends AbstractAntlrParser {

	@Inject
	private JBCGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalJBCParser createParser(XtextTokenStream stream) {
		return new InternalJBCParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "Project";
	}

	public JBCGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(JBCGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
